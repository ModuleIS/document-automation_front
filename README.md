Document_flow
=========

## Demo
https://document-automation-front.vercel.app/

## Installation

Clone the repository from Github
```
$ git clone https://gitlab.com/ModuleIS/document-automation_front.git
```

## NPM Scripts
- Install dependencies npm install
- run project npm start
- build a project npm run build