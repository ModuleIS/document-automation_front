import { all, fork } from 'redux-saga/effects';

import { sagaAuth } from 'states/ducks/auth/sagas';
import { sagaCreateUser } from 'states/ducks/user/sagas';
import { sagaUsers } from 'states/ducks/users/sagas';
import { sagaPositions } from 'states/ducks/positions/sagas';
import { sagaAccessGroups } from './ducks/access-groups/sagas';
import { sagaDocumentsTypes } from './ducks/documents-types/sagas';

const sagas = [
  sagaAuth,
  sagaCreateUser,
  sagaUsers,
  sagaPositions,
  sagaDocumentsTypes,
  sagaAccessGroups,
];

export default function* rootSaga() {
  yield all(sagas.map(fork));
}
