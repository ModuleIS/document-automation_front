import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';

import saga from 'states/saga';
import { middlewareLogin } from 'states/utils/middlewares/auth';
import { middlewareFilter } from 'states/utils/middlewares/filter';
import * as reducers from 'states/ducks';

const rootReducer = combineReducers(reducers);
const sagaMiddleware = createSagaMiddleware();

const composeEnhancers =
  (window && (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;
const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(sagaMiddleware, middlewareLogin, middlewareFilter)),
);

sagaMiddleware.run(saga);

export default store;
export type RootState = ReturnType<typeof rootReducer>;
