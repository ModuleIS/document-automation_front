import axios from 'axios';
import session from 'utilities/helpers/sessionAuth';

axios.interceptors.request.use(
  async (config) => {
    const token = session.getAccess();
    if (token) {
      config.headers['Authorization'] = token;
    }

    config.headers['Content-Type'] = 'application/json';
    return config;
  },
  (error) => {
    Promise.reject(error);
  },
);

export { axios };
