import { axios } from '../axiosInterceptor';
import { IResponsePositions } from 'states/ducks/positions/index.d';

export const getAllPositions = async (): Promise<any> => {
  try {
    const { data } = await axios.get<Array<IResponsePositions>>('http://94.153.212.68:5000/api/position/all');

    return data;
  } catch (err) {
    throw new Error(JSON.stringify({ message: err.response.data }));
  }
};

export const getPositionsWithPagination = async (params: string): Promise<any> => {
  try {
    const pageParams = params ? params : '?page=0&size=5';
    const { data } = await axios.get<Array<IResponsePositions>>(`http://94.153.212.68:5000/api/position${pageParams}`);

    return data;
  } catch (err) {
    throw new Error(JSON.stringify({ message: err.response.data }));
  }
};

export const createPosition = async (postData: { name: string; id?: number }): Promise<any> => {
  try {
    const { data } = await axios.post<{ name: string; id: number }>('http://94.153.212.68:5000/api/position/create', postData);

    return data;
  } catch (err) {
    throw new Error(JSON.stringify({ message: err.response.data }));
  }
};

export const updatePosition = async (postData: IResponsePositions): Promise<IResponsePositions> => {
  try {
    const { data } = await axios.put<IResponsePositions>('http://94.153.212.68:5000/api/position/update', postData);
    return data;
  } catch (err) {
    throw new Error(JSON.stringify({ message: err.response.data }));
  }
};

export const deletePosition = async (id: number): Promise<any> => {
  try {
    const { data } = await axios.delete(`http://94.153.212.68:5000/api/position/${id}`);
    return data;
  } catch (err) {
    throw new Error(err.response.status);
  }
};
