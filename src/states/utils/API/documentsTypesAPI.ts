import axios from 'axios';

export const fetchDocumentsTypes = async (): Promise<any> => {
  try {
    const { data } = await axios.get('http://94.153.212.68:5000/api/document-type');
    return data;
  } catch (err) {
    throw new Error(JSON.stringify(err.data));
  }
};

export const fetchAllDocumentsTypes = async (): Promise<any> => {
  try {
    const { data } = await axios.get('http://94.153.212.68:5000/api/document-type/all');
    return data;
  } catch (err) {
    throw new Error(JSON.stringify(err.data));
  }
};

export const fetchSelectedDocumentsType = async (id: number): Promise<any> => {
  try {
    const { data } = await axios.get(`http://94.153.212.68:5000/api/document-type/${id}`);
    return data;
  } catch (err) {
    throw new Error(JSON.stringify(err.data));
  }
};

export const fetchCreateDocumentsType = async (body: any): Promise<any> => {
  try {
    const { data } = await axios.post('http://94.153.212.68:5000/api/document-type/create', body);
    return data;
  } catch (err) {
    throw new Error(JSON.stringify(err.data));
  }
};

export const fetchUpdateDocumentsType = async (body: any): Promise<any> => {
  try {
    const { data } = await axios.put('http://94.153.212.68:5000/api/document-type/update', body);
    return data;
  } catch (err) {
    throw new Error(JSON.stringify(err.data));
  }
};

export const fetchDeleteDocumentsType = async (id: string): Promise<any> => {
  try {
    const { data } = await axios.delete(`http://94.153.212.68:5000/api/document-type/${id}`);
    return data;
  } catch (err) {
    throw new Error(JSON.stringify(err.data));
  }
};
