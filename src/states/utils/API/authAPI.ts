import { axios } from '../axiosInterceptor';

import { IRequestUser, IReponseData } from 'states/ducks/auth/index.d';

export const userLogin = async (user: IRequestUser): Promise<any> => {
  try {
    const { data } = await axios.post<IReponseData>('http://94.153.212.68:5000/api/auth/login', user);

    return data;
  } catch (err) {
    throw new Error(JSON.stringify({ message: err.response.data }));
  }
};

export const userLogout = async () => {
  try {
    await axios.post('http://94.153.212.68:5000/api/auth/logout');
  } catch (err) {
    throw new Error(JSON.stringify({ message: err.response.data }));
  }
};

export const resetPassword = async (postData: { email: string }): Promise<any> => {
  try {
    const { data } = await axios.put<boolean>('http://94.153.212.68:5000/api/password/reset', postData);

    return data;
  } catch (err) {
    throw new Error(JSON.stringify({ message: err.response.data }));
  }
};
