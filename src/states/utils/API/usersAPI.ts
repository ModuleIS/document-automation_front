import { axios } from '../axiosInterceptor';
import { IRequestUsers, IResponseData } from 'states/ducks/users/index.d';

export const getUsersAndFilterUsers = async(postData: IRequestUsers = {}, query: string = '?page=0&size=5'): Promise<any> => {
  try {
    const { data } = await axios.post<IResponseData>(`http://94.153.212.68:5000/api/user/get/filter/${query}&sort=firstName,asc`,postData,);

    return data;
  } catch (err) {
    throw new Error(JSON.stringify({ message: err.response.data }));
  }
};
