import { axios } from '../axiosInterceptor';
import { IResponseData } from 'states/ducks/users/index.d';

export const userCreate = async ({
  user: { email, firstname, password, patronymic, phone, role, secondname },
  id,
}: any): Promise<any> => {
  try {
    await axios.post<IResponseData>('http://94.153.212.68:5000/api/user/create', {
      email,
      firstname,
      password,
      patronymic,
      phone,
      positionid: id,
      role,
      secondname,
    });
  } catch (err) {
    throw new Error(JSON.stringify({ message: err.response.data }));
  }
};

export const getPasswod = async (): Promise<any> => {
  try {
    const { data } = await axios.get<string>('http://94.153.212.68:5000/api/password/generate');

    return data;
  } catch (err) {
    throw new Error(JSON.stringify({ message: err.response.data }));
  }
};
