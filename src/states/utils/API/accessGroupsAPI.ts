import axios from 'axios';

export const fetchAccessGroups = async (): Promise<any> => {
  try {
    const { data } = await axios.get('http://94.153.212.68:5000/api/permission');
    return data;
  } catch (err) {
    throw new Error(JSON.stringify(err.data));
  }
};
export const fetchSelectedAccessGroup = async (id: number): Promise<any> => {
  try {
    const { data } = await axios.get(`http://94.153.212.68:5000/api/permission/${id}`);
    return data;
  } catch (err) {
    throw new Error(JSON.stringify(err.data));
  }
};

export const fetchCreateAccessGroup = async (body: any): Promise<any> => {
  try {
    const { data } = await axios.post('http://94.153.212.68:5000/api/permission/create', body);
    return data;
  } catch (err) {
    throw new Error(JSON.stringify(err.data));
  }
};

export const fetchUpdateAccessGroups = async (body: any): Promise<any> => {
  try {
    const { data } = await axios.put('http://94.153.212.68:5000/api/permission/update', body);
    return data;
  } catch (err) {
    throw new Error(JSON.stringify(err.data));
  }
};
export const deleteAccessGroups = async (id: string): Promise<any> => {
  try {
    const { data } = await axios.delete(`http://94.153.212.68:5000/api/permission/${id}`);
    return data;
  } catch (err) {
    throw new Error(JSON.stringify(err.data));
  }
};

// GET FOLDER REQUEST
export const fetchFolders = async (): Promise<any> => {
  try {
    const { data } = await axios.get(`http://94.153.212.68:5000/api/folder`);
    return data;
  } catch (err) {
    throw new Error(JSON.stringify(err.data));
  }
};
