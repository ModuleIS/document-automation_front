import { Middleware } from 'redux';
import storage from 'utilities/helpers/storage';

export const middlewareFilter: Middleware = () => (next) => (action) => {
  if (action.type === '@users/REQUEST') {
    storage.delete('filter_users');
  }

  if (action.type === '@users/FILTER') {
    const { data } = action.payload;

    if (data && !data.hasOwnProperty('query')) {
      storage.set('filter_users', data);
    }

    data ?? storage.delete('filter_users'); 
  }

  return next(action);
};
