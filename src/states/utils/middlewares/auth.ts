import { Middleware } from 'redux';
import session from 'utilities/helpers/sessionAuth';

export const middlewareLogin: Middleware = () => (next) => (action) => {
  if (action.type === '@login/USER_SUCCESS') {
    const { secondName, token } = action.payload;
    session.setCookies(token, secondName, { expires: 7 });
  }

  next(action);
};
