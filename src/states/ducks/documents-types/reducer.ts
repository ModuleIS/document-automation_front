import { IDocumentsTypesState } from './index.d';
import { DocumentsTypesActions } from './action';

const initialState: IDocumentsTypesState = {
  documentsTypes: [],
  selectedDocumentsType: {},
  loading: false,
  error: null,
  folders: [],
};

const reducer = (
  state: IDocumentsTypesState = initialState,
  action: DocumentsTypesActions,
): IDocumentsTypesState => {
  switch (action.type) {
    case '@documentsTypes/REQUEST':
      return {
        ...state,
        loading: true,
      };
    case '@documentsTypes/ADD':
      return {
        ...state,
        loading: false,
        documentsTypes: action.payload.content ? action.payload.content : action.payload,
      };

    case '@documentsTypes/ADD_DOCUMENTS_TYPE':
      return {
        ...state,
        selectedDocumentsType: action.payload,
      };

    case '@documentsTypes/ADD_FOLDERS':
      return {
        ...state,
        folders: action.payload,
      };

    case '@documentsTypes/ERROR':
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
