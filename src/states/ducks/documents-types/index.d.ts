import {
  ADD,
  ADD_FOLDERS,
  CREATE,
  DELETE,
  GET_FOLDERS,
  REQUEST,
  ERROR,
  UPDATE,
  ADD_DOCUMENTS_TYPE,
  GET_DOCUMENTS_TYPE,
} from './types';

export interface IDocumentsTypesState {
  documentsTypes: any;
  selectedDocumentsType: any;
  loading: boolean;
  error: string | null;
  folders: any[];
}

export interface IFetchDocumentsTypes {
  type: typeof REQUEST;
}
export interface ISetDocumentsTypes {
  type: typeof ADD;
  payload: any;
}
export interface ICreateDocumentsType {
  type: typeof CREATE;
  payload: any;
}

export interface IUpdateDocumentsType {
  type: typeof UPDATE;
  payload: any;
}
export interface IGetDocumentsType {
  type: typeof GET_DOCUMENTS_TYPE;
  payload: any;
}
export interface IAddDocumentsType {
  type: typeof ADD_DOCUMENTS_TYPE;
  payload: any;
}

export interface IDeleteDocumentsType {
  type: typeof DELETE;
  payload: number;
}
export interface IAddFolders {
  type: typeof ADD_FOLDERS;
  payload: any;
}
export interface IDocumentsType {
  id: number;
  name: string;
}
export interface IDocumentsTypesErrorAction {
  type: typeof ERROR;
  payload: string;
}

export interface IFoldersAsync {
  type: typeof GET_FOLDERS;
}
