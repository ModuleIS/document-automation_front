import {
  IAddDocumentsType,
  IAddFolders,
  ICreateDocumentsType,
  IDeleteDocumentsType,
  IDocumentsTypesErrorAction,
  IFetchDocumentsTypes,
  IFoldersAsync,
  IGetDocumentsType,
  ISetDocumentsTypes,
  IUpdateDocumentsType,
} from './index.d';
import {
  GET_FOLDERS,
  ADD_FOLDERS,
  REQUEST,
  ADD,
  CREATE,
  UPDATE,
  DELETE,
  ERROR,
  GET_DOCUMENTS_TYPE,
  ADD_DOCUMENTS_TYPE,
} from './types';

export const getDocumentsTypes = (): IFetchDocumentsTypes => {
  return {
    type: REQUEST,
  };
};

export const setDocumentTypes = (documentsTypes: any[]): ISetDocumentsTypes => {
  return {
    type: ADD,
    payload: documentsTypes,
  };
};

export const createDocumentType = (newDocumentType: any): ICreateDocumentsType => {
  return {
    type: CREATE,
    payload: newDocumentType,
  };
};

export const updateDocumentType = (updatedType: any): IUpdateDocumentsType => {
  return {
    type: UPDATE,
    payload: updatedType,
  };
};
export const deleteDocumentType = (id: number): IDeleteDocumentsType => {
  return {
    type: DELETE,
    payload: id,
  };
};

export const getDocumentType = (id: number): IGetDocumentsType => {
  return {
    type: GET_DOCUMENTS_TYPE,
    payload: id,
  };
};

export const addSelectedDocumentType = (documentType: any): IAddDocumentsType => {
  return {
    type: ADD_DOCUMENTS_TYPE,
    payload: documentType,
  };
};

export const getFoldersAsync = (): IFoldersAsync => {
  return {
    type: GET_FOLDERS,
  };
};

export const addFolders = (folders: any): IAddFolders => {
  return {
    type: ADD_FOLDERS,
    payload: folders,
  };
};

export const setDocumentsTypesError = (error: string): IDocumentsTypesErrorAction => {
  return {
    type: ERROR,
    payload: error,
  };
};

export type DocumentsTypesActions =
  | IFetchDocumentsTypes
  | ISetDocumentsTypes
  | IAddDocumentsType
  | IAddFolders
  | IDocumentsTypesErrorAction;
