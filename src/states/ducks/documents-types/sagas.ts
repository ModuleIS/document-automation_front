import { all, call, put, select, takeLatest } from 'redux-saga/effects';
import { CREATE, DELETE, REQUEST, UPDATE } from './types';
import { setDocumentsTypesError, setDocumentTypes } from './action';

import { RootState } from 'states/store';
import {
  fetchCreateDocumentsType,
  fetchDeleteDocumentsType,
  fetchDocumentsTypes,
  fetchUpdateDocumentsType,
} from '../../utils/API/documentsTypesAPI';

export const getAllDocumentsTypes = (state: RootState) => state.documentsTypes.documentsTypes;

function* getDocumentsTypes() {
  try {
    const data = yield call(fetchDocumentsTypes);
    yield put(setDocumentTypes(data));
  } catch (err) {
    const { message } = JSON.parse(err.message);
    yield put(setDocumentsTypesError(message));
  }
}

function* createDocumentsType({ payload }: any) {
  const body = payload;
  try {
    const data = yield call(fetchCreateDocumentsType, body);
    const documentsTypes = yield select(getAllDocumentsTypes);
    yield put(setDocumentTypes([...documentsTypes, data]));
  } catch (err) {
    const { message } = JSON.parse(err.message);
    yield put(setDocumentsTypesError(message));
  }
}

function* updateDocumentsType({ payload }: any) {
  const body = payload;
  try {
    const data = yield call(fetchUpdateDocumentsType, body);
    const documentsTypes = yield select(getAllDocumentsTypes);
    const newAccessGroups = documentsTypes.map((item: { id: number }) =>
      item.id === data.id ? data : item,
    );
    yield put(setDocumentTypes(newAccessGroups));
  } catch (err) {
    const { message } = JSON.parse(err.message);
    yield put(setDocumentsTypesError(message));
  }
}

function* deleteDocumentsType({ payload }: any) {
  const id = payload;
  try {
    yield call(fetchDeleteDocumentsType, id);
    const documentsTypes = yield select(getAllDocumentsTypes);
    const filteredDocumentsTypes = documentsTypes.filter((item: { id: number }) => item.id !== id);
    yield put(setDocumentTypes(filteredDocumentsTypes));
  } catch (err) {
    const { message } = JSON.parse(err.message);
    yield put(setDocumentsTypesError(message));
  }
}

function* watchAllSagas() {
  yield all([
    takeLatest(REQUEST, getDocumentsTypes),
    takeLatest(CREATE, createDocumentsType),
    takeLatest(DELETE, deleteDocumentsType),
    takeLatest(UPDATE, updateDocumentsType),
  ]);
}

export { watchAllSagas as sagaDocumentsTypes };
