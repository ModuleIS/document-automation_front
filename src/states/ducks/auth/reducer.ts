import { ILoginState } from './index.d';
import { LoginActions } from './action';
import session from 'utilities/helpers/sessionAuth';

export const initalState: ILoginState = {
  user: session.getUser() ?? null,
  isAuthenticated: session.getHasAccess(),
  error: null,
};

const reducer = (state: ILoginState = initalState, action: LoginActions): ILoginState => {
  switch (action.type) {
    case '@login/USER_REQUEST':
      return {
        ...state,
        user: action.payload,
        error: null
      };
    case '@login/USER_SUCCESS':
      const { secondName: user, isAuthenticated } = action.payload;

      return {
        ...state,
        user,
        isAuthenticated,
      };
    case '@login/USER_ERROR':
      return {
        ...state,
        isAuthenticated: false,
        error: action.payload
      };
    case '@login/USER_LOGOUT':
      return {
        ...state,
        isAuthenticated: false,
      };
    case "@login/CLEAR_ERROR":
      return {
        ...state,
        isAuthenticated: false,
        error: null
      }
    default:
      return state;
  }
};

export default reducer;
