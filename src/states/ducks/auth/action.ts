import { USER_REQUEST, USER_SUCCESS, USER_ERROR, CLEAR_ERROR, USER_LOGOUT } from './types';
import { IRequestUser } from './index.d';

export interface IRequesUserAction {
  type: typeof USER_REQUEST;
  payload: any;
}

export interface ISetUserAction {
  type: typeof USER_SUCCESS;
  payload: any;
}

export interface ISetErrorAction {
  type: typeof USER_ERROR;
  payload: any;
}

export interface IClearUserErrorrAction {
  type: typeof CLEAR_ERROR;
}

export interface ISetUserLogoutAction {
  type: typeof USER_LOGOUT;
}

export const requestUserLogin = (data: IRequestUser): IRequesUserAction => {
  return {
    type: USER_REQUEST,
    payload: data,
  };
};

export const setUser = (data: { user: string; isAuthenticated: boolean }): ISetUserAction => {
  return {
    type: USER_SUCCESS,
    payload: data,
  };
};

export const setUserError = (error: string): ISetErrorAction => {
  return {
    type: USER_ERROR,
    payload: error,
  };
};

export const clearUserErrors = (): IClearUserErrorrAction => {
  return {
    type: CLEAR_ERROR
  };
};

export const setUserLogout = (): ISetUserLogoutAction => {
  return {
    type: USER_LOGOUT,
  };
};

export type LoginActions =
  | IRequesUserAction
  | ISetUserAction
  | ISetErrorAction
  | IClearUserErrorrAction
  | ISetUserLogoutAction;
