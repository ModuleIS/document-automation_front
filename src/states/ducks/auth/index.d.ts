export interface IRequestUser {
  firstName: string;
  patronymic: string;
  secondName: string;
  password: string;
}

export interface IReponseData {
  secondname: string;
  token: string;
}

export interface ILoginState {
  user: string | null;
  isAuthenticated: boolean;
  error: string | null;
}
