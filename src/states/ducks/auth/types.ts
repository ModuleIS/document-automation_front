export const USER_REQUEST = '@login/USER_REQUEST';
export const USER_SUCCESS = '@login/USER_SUCCESS';
export const USER_ERROR = '@login/USER_ERROR';
export const CLEAR_ERROR = '@login/CLEAR_ERROR';
export const USER_LOGOUT = '@login/USER_LOGOUT';
