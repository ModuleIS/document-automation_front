import { take, fork, call, put, takeLatest, all } from 'redux-saga/effects';

import { USER_REQUEST, USER_LOGOUT } from './types';
import { IRequestUser } from './index.d';
import { userLogin, userLogout } from 'states/utils/API/authAPI';
import { setUser, setUserError } from 'states/ducks/auth/action';
import session from 'utilities/helpers/sessionAuth';

function* authorization(user: IRequestUser) {
  try {
    const data = yield call(userLogin, user);
    yield put(
      setUser({
        ...data,
        isAuthenticated: true,
      }),
    );
  } catch (err) {
    const { message } = JSON.parse(err.message);
    yield put(setUserError(message));
  }
}

function* watchSagaAuth() {
  while (true) {
    const { payload } = yield take(USER_REQUEST);
    yield fork(authorization, payload);
  }
}

function* userLogout2() {
  yield call(userLogout);
  session.deleteCookies();

}

function* watchSagaAuth2() {
  yield takeLatest(USER_LOGOUT, userLogout2)
}

function* watchFoo() {
  yield all([
    fork(watchSagaAuth),
    fork(watchSagaAuth2),
  ]);
}

export { watchFoo as sagaAuth };
