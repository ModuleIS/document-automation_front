import {
  ADD,
  ADD_ACCESS_GROUP,
  ADD_FOLDERS,
  CREATE,
  DELETE,
  GET_ACCESS_GROUP,
  GET_FOLDERS,
  REQUEST,
  ERROR,
} from './types';

export interface IAccessGroupsState {
  accessGroups: any;
  selectedAccessGroup: any;
  loading: boolean;
  error: string | null;
  folders: any[];
}

export interface IFetchAccessGroups {
  type: typeof REQUEST;
}
export interface ISetAccessGroupsAction {
  type: typeof ADD;
  payload: any;
}
export interface ICreateNewAccessGroups {
  type: typeof CREATE;
  payload: any;
}

export interface IUpdateAccessGroups {
  type: typeof UPDATE;
  payload: any;
}

export interface IDeleteAccessGroup {
  type: typeof DELETE;
  payload: number;
}

export interface IGetAccessGroup {
  type: typeof GET_ACCESS_GROUP;
  payload: number;
}
export interface IAddAccessGroup {
  type: typeof ADD_ACCESS_GROUP;
  payload: any;
}

export interface IAddFolders {
  type: typeof ADD_FOLDERS;
  payload: any;
}
export interface IAccessGroupErrorAction {
  type: typeof ERROR;
  payload: string;
}

export interface IFoldersAsync {
  type: typeof GET_FOLDERS;
}
