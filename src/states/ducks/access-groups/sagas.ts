import { all, call, put, select, takeLatest } from 'redux-saga/effects';
import * as api from 'states/utils/API/accessGroupsAPI';
import { CREATE, GET_ACCESS_GROUP, GET_FOLDERS, DELETE, REQUEST, UPDATE } from './types';
import { addFolders, addSelectedAccessGroup, setAccessGroupError, setAccessGroups } from './action';
import {
  fetchAccessGroups,
  fetchCreateAccessGroup,
  fetchFolders,
  fetchSelectedAccessGroup,
  fetchUpdateAccessGroups,
} from 'states/utils/API/accessGroupsAPI';
import { RootState } from 'states/store';

export const getAllAccessGroups = (state: RootState) => state.accessGroups.accessGroups;

function* getAccessGroups() {
  try {
    const data = yield call(fetchAccessGroups);
    yield put(setAccessGroups(data));
  } catch (err) {
    const { message } = JSON.parse(err.message);
    yield put(setAccessGroupError(message));
  }
}

function* createAccessGroup({ payload }: any) {
  const body = payload;
  try {
    const data = yield call(fetchCreateAccessGroup, body);
    const accessGroups = yield select(getAllAccessGroups);
    yield put(setAccessGroups([...accessGroups, data]));
  } catch (err) {
    const { message } = JSON.parse(err.message);
    yield put(setAccessGroupError(message));
  }
}

function* updateAccessGroup({ payload }: any) {
  const body = payload;
  try {
    const data = yield call(fetchUpdateAccessGroups, body);
    const accessGroups = yield select(getAllAccessGroups);
    const newAccessGroups = accessGroups.map((item: { id: number }) =>
      item.id === data.id ? data : item,
    );
    yield put(setAccessGroups(newAccessGroups));
  } catch (err) {
    const { message } = JSON.parse(err.message);
    yield put(setAccessGroupError(message));
  }
}

function* deleteAccessGroup({ payload }: any) {
  const id = payload;
  try {
    yield call(api.deleteAccessGroups, id);
    const accessGroups = yield select(getAllAccessGroups);
    const filteredAccessGroups = accessGroups.filter((item: { id: number }) => item.id !== id);
    yield put(setAccessGroups(filteredAccessGroups));
  } catch (err) {
    const { message } = JSON.parse(err.message);
    yield put(setAccessGroupError(message));
  }
}

function* getSelectedAccessGroup({ payload }: any) {
  const id = payload;
  try {
    const data = yield call(fetchSelectedAccessGroup, id);
    yield put(addSelectedAccessGroup(data));
  } catch (err) {
    const { message } = JSON.parse(err.message);
    yield put(setAccessGroupError(message));
  }
}

function* getFolders() {
  try {
    const data = yield call(fetchFolders);
    yield put(addFolders(data));
  } catch (err) {
    const { message } = JSON.parse(err.message);
    yield put(setAccessGroupError(message));
  }
}

function* watchAllSagas() {
  yield all([
    takeLatest(REQUEST, getAccessGroups),
    takeLatest(CREATE, createAccessGroup),
    takeLatest(DELETE, deleteAccessGroup),
    takeLatest(UPDATE, updateAccessGroup),
    takeLatest(GET_ACCESS_GROUP, getSelectedAccessGroup),
    takeLatest(GET_FOLDERS, getFolders),
  ]);
}

export { watchAllSagas as sagaAccessGroups };
