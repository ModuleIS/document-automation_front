import { IAccessGroupsState } from './index.d';
import { AccessGroupsActions } from './action';

const initialState: IAccessGroupsState = {
  accessGroups: [],
  selectedAccessGroup: {},
  loading: false,
  error: null,
  folders: [],
};

const reducer = (
  state: IAccessGroupsState = initialState,
  action: AccessGroupsActions,
): IAccessGroupsState => {
  switch (action.type) {
    case '@accessGroups/REQUEST':
      return {
        ...state,
        loading: true,
      };
    case '@accessGroups/ADD':
      return {
        ...state,
        loading: false,
        accessGroups: action.payload.content ? action.payload.content : action.payload,
      };

    case '@accessGroups/ADD_ACCESS_GROUP':
      return {
        ...state,
        selectedAccessGroup: action.payload,
      };

    case '@accessGroups/ADD_FOLDERS':
      return {
        ...state,
        folders: action.payload,
      };

    case '@accessGroups/ERROR':
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
