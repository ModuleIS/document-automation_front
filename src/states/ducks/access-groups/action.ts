import {
  IAccessGroupErrorAction,
  IAddAccessGroup,
  IAddFolders,
  ICreateNewAccessGroups,
  IDeleteAccessGroup,
  IFetchAccessGroups,
  IFoldersAsync,
  IGetAccessGroup,
  ISetAccessGroupsAction,
  IUpdateAccessGroups,
} from './index.d';
import {
  GET_ACCESS_GROUP,
  GET_FOLDERS,
  ADD_FOLDERS,
  REQUEST,
  ADD,
  CREATE,
  UPDATE,
  DELETE,
  ADD_ACCESS_GROUP,
  ERROR,
} from './types';

export const getAccessGroups = (): IFetchAccessGroups => {
  return {
    type: REQUEST,
  };
};

export const setAccessGroups = (accessGroups: any[]): ISetAccessGroupsAction => {
  return {
    type: ADD,
    payload: accessGroups,
  };
};

export const createAccessGroups = (newAccessGroups: any): ICreateNewAccessGroups => {
  return {
    type: CREATE,
    payload: newAccessGroups,
  };
};

export const updateAccessGroups = (updatedGroup: any): IUpdateAccessGroups => {
  return {
    type: UPDATE,
    payload: updatedGroup,
  };
};
export const deleteAccessGroup = (id: number): IDeleteAccessGroup => {
  return {
    type: DELETE,
    payload: id,
  };
};

export const getAccessGroupAsync = (id: number): IGetAccessGroup => {
  return {
    type: GET_ACCESS_GROUP,
    payload: id,
  };
};

export const addSelectedAccessGroup = (accessGroup: any): IAddAccessGroup => {
  return {
    type: ADD_ACCESS_GROUP,
    payload: accessGroup,
  };
};

export const getFoldersAsync = (): IFoldersAsync => {
  return {
    type: GET_FOLDERS,
  };
};

export const addFolders = (folders: any): IAddFolders => {
  return {
    type: ADD_FOLDERS,
    payload: folders,
  };
};

export const setAccessGroupError = (error: string): IAccessGroupErrorAction => {
  return {
    type: ERROR,
    payload: error,
  };
};

export type AccessGroupsActions =
  | IFetchAccessGroups
  | ISetAccessGroupsAction
  | IAddAccessGroup
  | IAddFolders
  | IAccessGroupErrorAction;
