import { ICreateUserState } from './index.d';
import { CreateUserActions } from './action';

const initalState: ICreateUserState = {
  successfully: false,
  loadingUser: false,
  errorUser: null,
};

const reducer = (
  state: ICreateUserState = initalState,
  action: CreateUserActions,
): ICreateUserState => {
  switch (action.type) {
    case '@user/REQUEST':
      return {
        ...state,
        successfully: false,
        errorUser: null,
        loadingUser: true,
      };
    case '@user/SUCCESS':
      return {
        ...state,
        successfully: true,
        loadingUser: false,
      };
    case '@user/DISCARD':
      return {
        ...state,
        successfully: false,
        errorUser: null,
      };
    case '@user/ERROR':
      return {
        successfully: false,
        loadingUser: false,
        errorUser: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;
