import { REQUEST, SUCCESS, ERROR, DISCARD } from './types';

export interface IUserCreateRequestAction {
  type: typeof REQUEST;
  payload: any;
}

export interface ICreateUserSuccessAction {
  type: typeof SUCCESS;
}

export interface ICreateUserErrorAction {
  type: typeof ERROR;
  payload: any;
}

export interface IDiscardAction {
  type: typeof DISCARD;
}

export interface IUser {
  firstname: string;
  secondname: string;
  patronymic: string;
  email: string;
  phone: string;
  position: string;
  password: string;
  role: string;
}

export interface ICreateUserState {
  successfully: boolean;
  loadingUser: boolean;
  errorUser: null | string;
}
