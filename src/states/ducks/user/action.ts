import {
  IUser,
  ICreateUserSuccessAction,
  ICreateUserErrorAction,
  IUserCreateRequestAction,
  IDiscardAction,
} from './index.d';
import { REQUEST, SUCCESS, ERROR, DISCARD } from './types';

export const createUserAsync = (user: IUser): IUserCreateRequestAction => {
  return {
    type: REQUEST,
    payload: user,
  };
};

export const setSuccessCreateUser = (): ICreateUserSuccessAction => {
  return {
    type: SUCCESS,
  };
};

export const setCreateUserError = (error: string): ICreateUserErrorAction => {
  return {
    type: ERROR,
    payload: error,
  };
};

export const setDiscard = (): IDiscardAction => {
  return {
    type: DISCARD,
  };
};

export type CreateUserActions =
  | IUserCreateRequestAction
  | ICreateUserSuccessAction
  | ICreateUserErrorAction
  | IDiscardAction;
