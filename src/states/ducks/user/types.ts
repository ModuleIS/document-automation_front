export const REQUEST = '@user/REQUEST';
export const SUCCESS = '@user/SUCCESS';
export const DISCARD = '@user/DISCARD';
export const ERROR = '@user/ERROR';
