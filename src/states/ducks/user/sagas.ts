import {
  call,
  fork,
  take,
  select,
  TakeEffect,
  ForkEffect,
  put,
  SelectEffect,
  CallEffect,
  PutEffect
} from 'redux-saga/effects';

import { REQUEST } from './types';
import { ICreateUserSuccessAction, ICreateUserErrorAction, IUser } from './index.d';
import { IResponsePositions } from 'states/ducks/positions/index.d';
import { setSuccessCreateUser, setCreateUserError } from './action';
import { userCreate } from 'states/utils/API/createUserAPI';
import { getAllPoss } from 'states/ducks/positions/sagas';

function* createUser(user: IUser): Generator<SelectEffect| CallEffect<any>| PutEffect<ICreateUserSuccessAction>| PutEffect<ICreateUserErrorAction>,void,Array<IResponsePositions>> {
  try {
    const positions = yield select(getAllPoss);
    const { id }: any = positions.find((i: any) => i.name === user.position);

    yield call(userCreate, {
      user,
      id,
    });

    yield put(setSuccessCreateUser());
  } catch (err) {
    const { message } = JSON.parse(err.message);
    yield put(setCreateUserError(message));
  }
}

function* watchSagaCreateUser(): Generator<TakeEffect | ForkEffect<void>, void, { payload: any }> {
  while (true) {
    const { payload } = yield take(REQUEST);
    yield fork(createUser, payload);

    // disabled user
  }
}

export { watchSagaCreateUser as sagaCreateUser };
