export const REQUEST = '@positions/REQUEST';
export const REQUEST_PAGINATION = '@positions/REQUEST_PAGINATION';
export const ADD_PAGINATION = '@positions/ADD_PAGINATION';
export const CREATE = '@position/CREATE';
export const ADD = '@positions/ADD';
export const ERROR = '@positions/ERROR';
export const DELETE = '@positions/DELETE';
export const UPDATE = '@positions/UPDATE';
export const ADD_CHANGES = '@positions/ADD_CHANGES';
