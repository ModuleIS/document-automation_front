import {
  REQUEST,
  CREATE,
  ADD,
  ERROR,
  DELETE,
  UPDATE,
  REQUEST_PAGINATION,
  ADD_PAGINATION,
  ADD_CHANGES,
} from './types';

export interface IPositionsRequestAction {
  type: typeof REQUEST;
}
export interface IPositionsPaginationRequest {
  type: typeof REQUEST_PAGINATION;
  payload: string;
}

export interface ICreatePositionAction {
  type: typeof CREATE;
  payload: any;
}

export interface IUpdatePositionAction {
  type: typeof UPDATE;
  payload: IResponsePositions;
}

export interface IPositionsSuccessAction {
  type: typeof ADD;
  payload: any;
}
export interface ISetPositionsPagination {
  type: typeof ADD_PAGINATION;
  payload: any;
}
export interface ISetPositionsChanges {
  type: typeof ADD_CHANGES;
  payload: any;
}

export interface IPositionsErrorAction {
  type: typeof ERROR;
  payload: any;
}

interface IResponsePositions {
  id: number;
  name: string;
}
interface IDeletePositionAction {
  type: typeof DELETE;
  payload: number;
}

export interface IPositionsState {
  positions: Array<IResponsePositions>;
  positionsFiltered: Array<IResponsePositions>;
  loadingPositions: boolean;
  errorPositions: string | null;
  totalPage: number;
}
