import { createSelector } from 'reselect';
import { RootState } from 'states/store';

const selectPositions = (state: RootState) => state.positions;

export const getComputedPositions = createSelector(selectPositions, ({ positions, ...rest }) => {
  const result = positions.map(({ name, id }) => ({ label: name, value: name, id }));

  return {
    ...rest,
    positions: result,
  };
});
