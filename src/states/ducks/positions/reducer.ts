import { IPositionsState } from './index.d';
import { PositionActions } from './action';

const initialState: IPositionsState = {
  positions: [],
  positionsFiltered: [],
  totalPage: 0,
  loadingPositions: false,
  errorPositions: null,
};

const reducer = (
  state: IPositionsState = initialState,
  action: PositionActions,
): IPositionsState => {
  switch (action.type) {
    case '@positions/REQUEST':
    case '@positions/REQUEST_PAGINATION':
    case '@position/CREATE':
      return {
        ...state,
        errorPositions: null,
        loadingPositions: true,
      };
    case '@positions/ADD':
      return {
        ...state,
        loadingPositions: false,
        positions: action.payload,
      };
    case '@positions/ADD_CHANGES':
      return {
        ...state,
        positionsFiltered: action.payload,
      };

    case '@positions/ADD_PAGINATION':
      const { content, totalelements } = action.payload;
      return {
        ...state,
        positionsFiltered: content,
        totalPage: totalelements,
        loadingPositions: false,
      };
    case '@positions/ERROR':
      return {
        ...state,
        loadingPositions: false,
        errorPositions: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;
