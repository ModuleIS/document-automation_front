import {
  IPositionsRequestAction,
  ICreatePositionAction,
  IPositionsSuccessAction,
  IPositionsErrorAction,
  IResponsePositions,
  IUpdatePositionAction,
  IDeletePositionAction,
  IPositionsPaginationRequest,
  ISetPositionsPagination,
  ISetPositionsChanges,
} from './index.d';
import {
  REQUEST,
  CREATE,
  ADD,
  ERROR,
  DELETE,
  UPDATE,
  REQUEST_PAGINATION,
  ADD_PAGINATION,
  ADD_CHANGES,
} from './types';

export const positionsAsync = (): IPositionsRequestAction => {
  return {
    type: REQUEST,
  };
};
export const getPositionsWithPagination = (params: string): IPositionsPaginationRequest => {
  return {
    type: REQUEST_PAGINATION,
    payload: params,
  };
};

export const setPositionsWithPagination = (positions: any): ISetPositionsPagination => {
  return {
    type: ADD_PAGINATION,
    payload: positions,
  };
};

export const addEditedPositions = (positions: any): ISetPositionsChanges => {
  return {
    type: ADD_CHANGES,
    payload: positions,
  };
};

export const createPositionAsync = (position: { name: string }): ICreatePositionAction => {
  return {
    type: CREATE,
    payload: position,
  };
};
export const updatePositionAsync = (position: {
  name: string;
  id: number;
}): IUpdatePositionAction => {
  return {
    type: UPDATE,
    payload: position,
  };
};

export const deletePositionAsync = (id: number): IDeletePositionAction => {
  return {
    type: DELETE,
    payload: id,
  };
};

export const setPositions = (positions: any): IPositionsSuccessAction => {
  // fix type positions
  return {
    type: ADD,
    payload: positions,
  };
};

export const setPositionsError = (error: string): IPositionsErrorAction => {
  return {
    type: ERROR,
    payload: error,
  };
};

export type PositionActions =
  | IPositionsRequestAction
  | ICreatePositionAction
  | IPositionsSuccessAction
  | IPositionsErrorAction
  | ISetPositionsPagination
  | IPositionsPaginationRequest
  | ISetPositionsChanges;
