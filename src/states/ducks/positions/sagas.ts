import {
  call,
  put,
  CallEffect,
  PutEffect,
  all,
  take,
  fork,
  select,
  AllEffect,
  ForkEffect,
  TakeEffect,
  SelectEffect,
  takeLatest,
} from 'redux-saga/effects';

import {
  addEditedPositions,
  setPositions,
  setPositionsError,
  setPositionsWithPagination,
} from './action';
import {
  getAllPositions,
  createPosition as createPostPosition,
  getPositionsWithPagination,
} from 'states/utils/API/positionsAPI';
import { deletePosition as deletePostPosition } from 'states/utils/API/positionsAPI';
import { updatePosition as updatePostPosition } from 'states/utils/API/positionsAPI';

import {
  IDeletePositionAction,
  IPositionsErrorAction,
  IPositionsPaginationRequest,
  IPositionsSuccessAction,
  IResponsePositions,
  ISetPositionsChanges,
  ISetPositionsPagination,
  IUpdatePositionAction,
} from './index.d';
import { REQUEST, CREATE, DELETE, UPDATE, REQUEST_PAGINATION } from './types';
import { RootState } from 'states/store';

export const getAllPoss = (state: RootState) => state.positions.positionsFiltered;

function* getPositions(): Generator<
  | CallEffect<Array<IResponsePositions>>
  | PutEffect<IPositionsSuccessAction>
  | PutEffect<IPositionsErrorAction>,
  void,
  Array<IResponsePositions>
> {
  try {
    const data = yield call(getAllPositions);
    yield put(setPositions(data));
  } catch (err) {
    const { message } = JSON.parse(err.message);
    yield put(setPositionsError(message));
  }
}

function* getPositionsPagination({
  payload: params,
}: IPositionsPaginationRequest): Generator<
  | CallEffect<Array<IResponsePositions>>
  | PutEffect<ISetPositionsPagination>
  | PutEffect<IPositionsErrorAction>,
  void,
  Array<IResponsePositions>
> {
  try {
    const data = yield call(getPositionsWithPagination, params);
    yield put(setPositionsWithPagination(data));
  } catch (err) {
    const { message } = JSON.parse(err.message);
    yield put(setPositionsError(message));
  }
}

function* createPosition(position: {
  name: string;
}): Generator<
  | PutEffect<ISetPositionsChanges>
  | CallEffect<IResponsePositions>
  | SelectEffect
  | PutEffect<IPositionsErrorAction>,
  void,
  Array<IResponsePositions>
> {
  try {
    const data = yield call(createPostPosition, position);
    const positions: Array<IResponsePositions> = yield select(getAllPoss);

    yield put(addEditedPositions([...positions, data]));
  } catch (err) {
    const { message } = JSON.parse(err.message);
    yield put(setPositionsError(message));
  }
}

function* updatePosition({ payload: position }: IUpdatePositionAction) {
  try {
    const data: IResponsePositions = yield call(updatePostPosition, position);
    const positions: IResponsePositions[] = yield select(getAllPoss);
    const newPositions = positions.map((item) => (item.id === data.id ? data : item));

    yield put(addEditedPositions(newPositions));
  } catch (err) {
    const { message } = JSON.parse(err.message);
    yield put(setPositionsError(message));
  }
}

function* deletePosition({
  payload: id,
}: IDeletePositionAction): Generator<
  | PutEffect<ISetPositionsChanges>
  | CallEffect<IResponsePositions>
  | SelectEffect
  | PutEffect<IPositionsErrorAction>,
  void,
  any
> {
  try {
    yield call(deletePostPosition, id);
    const positions: Array<IResponsePositions> = yield select(getAllPoss);
    const newPositions = positions.filter((item) => item.id !== id);
    yield put(addEditedPositions(newPositions));
  } catch (err) {
    yield put(setPositionsError(err.message));
  }
}

function* watchSagaPositions(): Generator<TakeEffect | ForkEffect<void>, void, unknown> {
  while (true) {
    yield take(REQUEST);
    yield fork(getPositions);
  }
}

function* watchSagaPosition(): Generator<
  TakeEffect | ForkEffect<void>,
  void,
  { payload: { name: string } }
> {
  while (true) {
    const { payload } = yield take(CREATE);
    yield fork(createPosition, payload);
  }
}

function* watchAllSagas(): Generator<AllEffect<ForkEffect<void>>, void, unknown> {
  yield all([
    fork(watchSagaPositions),
    fork(watchSagaPosition),
    takeLatest(REQUEST_PAGINATION, getPositionsPagination),
    takeLatest(DELETE, deletePosition),
    takeLatest(UPDATE, updatePosition),
  ]);
}

export { watchAllSagas as sagaPositions };
