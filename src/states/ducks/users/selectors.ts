import { createSelector } from 'reselect';
import { RootState } from 'states/store';

const selectUsers = (state: RootState) => state.users;

export const getUsersData = createSelector(selectUsers, ({ users, ...rest }) => {
  const userInitials = users.map(({ firstname, secondname, id }) => ({
    id: id,
    name: `${firstname} ${secondname}`,
  }));

  return {
    ...rest,
    user: userInitials,
  };
});
