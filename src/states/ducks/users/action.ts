import {
  IUsersRequestAction,
  IUsersSuccessAction,
  IFilterUsersAction,
  IRequestUsers,
  IResponseData,
} from './index.d';
import { REQUEST, ADD, FILTER } from './types';

export const usersAsync = (queryParams?: string): IUsersRequestAction => {
  return {
    type: REQUEST,
    payload: {
      data: undefined,
      queryParams,
    },
  };
};

export const filterUsersAsync = (filter: IRequestUsers): IFilterUsersAction => {
  return {
    type: FILTER,
    payload: {
      data: filter,
      queryParams: undefined,
    },
  };
};

export const setUsers = (users: IResponseData): IUsersSuccessAction => {
  return {
    type: ADD,
    payload: users,
  };
};

export type UsersActions = IUsersRequestAction | IUsersSuccessAction | IFilterUsersAction;
