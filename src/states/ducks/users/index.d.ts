import { REQUEST, ADD, FILTER } from './types';
import { IUser } from 'states/ducks/user/index.d';

export interface IUsersRequestAction {
  type: typeof REQUEST;
  payload: any;
}

export interface IUsersSuccessAction {
  type: typeof ADD;
  payload: any;
}

export interface IFilterUsersAction {
  type: typeof FILTER;
  payload: any;
}

export interface IRequestUsers {
  position?: string;
  query?: string;
  role?: string;
  status?: string;
}

interface IResponseUser extends IUser {
  creationdate: string;
  id: number;
  status: string;
}

export interface IResponseData {
  content: Array<IResponseUser>;
  empty: boolean;
  first: boolean;
  last: boolean;
  number: number;
  numberofelements: number;
  pageable: {
    offset: number;
    paged: boolean;
    pagenumber: number;
    pagesize: number;
    sort: {
      empty: boolean;
      sorted: boolean;
      unsorted: boolean;
    };
    unpaged: boolean;
  };
  size: number;
  sort: {
    empty: boolean;
    sorted: boolean;
    unsorted: boolean;
  };
  totalelements: number;
  totalpages: number;
}

export interface IUsersState {
  users: Array<IResponseUser>,
  totalPage: number,
  loading: boolean,
  pendingFilter: boolean,
  error: string | null
}
