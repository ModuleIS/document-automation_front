export const REQUEST = '@users/REQUEST';
export const ADD = '@users/ADD';
export const ERROR = '@users/ERROR';
export const FILTER = '@users/FILTER';
export const SEARCH = '@users/SEARCH';
