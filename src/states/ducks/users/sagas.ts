import { call, CallEffect, put, PutEffect, ForkEffect, takeLatest } from 'redux-saga/effects';

import { REQUEST, FILTER } from './types';
import { IUsersSuccessAction, IResponseData } from './index.d';
import { setUsers } from './action';
import { getUsersAndFilterUsers } from 'states/utils/API/usersAPI';

function* getUser({
  payload,
}: ReturnType<typeof setUsers>): Generator<
  CallEffect<any> | PutEffect<IUsersSuccessAction>,
  void,
  IResponseData
> {
  try {
    const { data: postData, queryParams } = payload;
    const data = yield call(getUsersAndFilterUsers, postData, queryParams);

    yield put(setUsers(data));
  } catch (err) {
    const { message } = JSON.parse(err.message);
    console.log(message);
  }
}

function* watchSagaUsers(): Generator<ForkEffect<never>, void, unknown> {
  yield takeLatest([REQUEST, FILTER], getUser);
}

export { watchSagaUsers as sagaUsers };
