import { IUsersState } from './index.d';
import { UsersActions } from './action';

const initialState: IUsersState = {
  users: [],
  totalPage: 0,
  loading: false,
  pendingFilter: false,
  error: null,
};

const reducer = (state: IUsersState = initialState, action: UsersActions): IUsersState => {
  switch (action.type) {
    case '@users/REQUEST':
      return {
        ...state,
        loading: true,
      };
    case '@users/ADD':
      const { content, totalelements } = action.payload;
      return {
        ...state,
        users: content,
        totalPage: totalelements,
        loading: false
      }
    default:
      return state;
  }
};

export default reducer;
