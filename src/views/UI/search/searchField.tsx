import React from 'react';
import { SearchField } from 'views/styled/SearchStyle';

const Search = () => {
  return (
    <SearchField
      placeholder="Пошук" 
      allowClear
    />
  );
};

export default Search;
