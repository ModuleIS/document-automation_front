import React, { SyntheticEvent } from 'react';
import { useField, useFormikContext } from 'formik';
import { Select } from 'antd';

import { FieldSelect } from 'views/styled/FieldsStyle';
import { FormItem } from 'views/styled/FieldsStyle';

const { Option } = Select;

const SelectField = ({ options, ...otherParams }: any) => {
  const [field, meta] = useField(otherParams);
  const { value } = field;
  const { error, touched } = meta;
  const { setFieldValue, setFieldTouched } = useFormikContext();

  const handleChange = (value: SyntheticEvent): void => {
    setFieldValue(otherParams.name, value);
  };

  const handleBlur = (): void => {
    setFieldTouched(otherParams.name, true);
  };

  return (
    <FormItem validateStatus={error && touched ? 'error' : 'success'} help={touched && error}>
      <FieldSelect
        {...otherParams}
        value={value || undefined}
        onChange={handleChange}
        onBlur={handleBlur}
      >
        {options.map(({ value, label, id }: any) => (
          <Option key={id} value={value}>
            {label}
          </Option>
        ))}
      </FieldSelect>
    </FormItem>
  );
};

export default SelectField;
