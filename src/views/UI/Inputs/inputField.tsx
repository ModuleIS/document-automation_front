import React from 'react';
import { useField, useFormikContext } from 'formik';
import PhoneInput from 'react-phone-input-2';

import { FormItem, FormInput } from 'views/styled/FieldsStyle';
import { DescriptionsContainer } from '../../styled/AccessGroupStyle';

interface InputFieldTypes {
  name: string;
  type: string;
  placeholder?: string;
  setValue?: (cb: (field: string, value: any) => void) => React.ReactNode;
  iconRender?: (visible: boolean) => React.ReactNode;
}

const InputField = ({ setValue = () => undefined, ...rest }: InputFieldTypes) => {
  const { name } = rest;
  const [field, meta] = useField(rest);
  const { value, onChange, onBlur } = field;
  const { error, touched } = meta;
  const { setFieldValue, setFieldTouched } = useFormikContext();

  const inputProps = {
    ...rest,
    value,
    onChange,
    onBlur,
  };

  const handleChange = (value: string): void => {
    setFieldValue('phone', value);
  };

  const handleBlur = (): void => {
    setFieldTouched('phone', true);
  };

  const Field = name !== 'password' ? FormInput : FormInput.Password;

  const fields = (): React.ReactNode => {
    if (name === 'descriptions') {
      return <DescriptionsContainer {...inputProps} />;
    } else if (name !== 'phone') {
      return <Field {...inputProps} prefix={setValue(setFieldValue)} />;
    }

    return (
      <PhoneInput
        value={value}
        onChange={handleChange}
        onBlur={handleBlur}
        country="ua"
        enableAreaCodes
        specialLabel=""
        placeholder="Телефон"
      />
    );
  };

  return (
    <FormItem validateStatus={error && touched ? 'error' : 'success'} help={touched && error}>
      {fields()}
    </FormItem>
  );
};

export default InputField;
