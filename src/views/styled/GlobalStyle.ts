import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  html {
    font-size: 100%;
    width: 100%;
    height: 100%;
    -webkit-text-size-adjust: 100%;
    -moz-text-size-adjust: 100%;
    box-sizing: border-box;
  }

  body {
    font-family: -apple-system, BlinkMacSystemFont, 'Roboto', 'Helvetica Neue', sans-serif;
    font-size: 16px;
    line-height: 1.52947;
    font-weight: normal;
    -webkit-font-smoothing: subpixel-antialiased;
    -moz-osx-font-smoothing: auto;
    -webkit-user-select: none;
    -webkit-tap-highlight-color: transparent;
    -webkit-touch-callout: none;
    direction: ltr;
    color: #323232;
    background-color: #fff;
    overflow-x: hidden;
  }

  #root, #shell, .root {
    min-width: 320px;
    width: 100%;
    height: 100%;
    -webkit-overflow-scrolling: touch;
  }

  button {
    background: transparent;
    padding: 0;
    border: 0;
  }

  img {
    display: inline-block;
    max-width: 100%;
    height: auto;
    vertical-align: middle;
    object-fit: cover;
  }

  mark {
    background-color: #ffc200;
    font-style: normal;
    font-weight: 500;
    padding: 0;
    letter-spacing: 0.35px;
  }

  .ant-modal-confirm .ant-modal-body {
    padding: 20px 45px;

    .ant-modal-confirm-body .ant-modal-confirm-title {
      font-size: 22px;
      text-align: center;
    }

    .ant-modal-confirm-content {
      margin-top: 15px;
      
      button {
        margin-top: 15px;
      }
    }

    .ant-modal-confirm-btns {
      display: none;
    }
  }
`;

export default GlobalStyle;
