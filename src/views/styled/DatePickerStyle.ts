import styled from 'styled-components';

export const DatePickerField = styled.div`
  margin-left: 32px;

  .ant-picker {
    font-size: 16px;
    border: 0;
  }

  .ant-picker-input > input::placeholder {
    font-size: 16px;
  }

  .ant-picker-focused {
    box-shadow: none;
  }

  .ant-picker-range .ant-picker-active-bar {
    background: #000;
  }
`;
