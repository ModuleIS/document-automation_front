import styled from 'styled-components';

export const Layout = styled.div`
  display: flex;
  width: 100%;
  height: calc(100% - 83px);
  max-width: 1440px;
  margin: auto;
  padding: 0 35px;
`;

export const MainContent = styled.main`
  width: calc(100% - 280px);
`;

export const ManageWrapper = styled.div`
  min-width: 1400px;
  height: 100%;
  background: linear-gradient(to bottom, #afffff, #fdffe0);
`;

export const Title = styled.h2`
  font-size: 24px;
  color: #000;
`;

export const Head = styled.div<{ bottom: number | null; isShow: boolean }>`
  display: ${({ isShow }) => (isShow ? 'flex' : 'none')};
  align-items: center;
  margin-bottom: ${({ bottom }) => (!bottom ? '0' : bottom + 'px')};

  span[role='button'] {
    margin-right: 25px;
    cursor: pointer;

    &:hover {
      opacity: 0.5;
    }
  }
`;

export const TopPanel = styled.div`
  padding: 50px 25px 12px 25px;
  background-color: #fff;
`;
