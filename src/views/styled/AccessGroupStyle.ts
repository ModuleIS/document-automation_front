import styled from 'styled-components';

export const DescriptionsContainer = styled.textarea`
  resize: none;
  wrap: soft;
  margin: 22px 0;
  height: 113px;
  overflow: hidden;
  width: 700px;
  border-radius: 20px;
  border: double 2px transparent;
  background-image: linear-gradient(white, white), linear-gradient(to right, #afffff, #faffa0);
  background-origin: border-box;
  background-clip: padding-box, border-box;
  padding: 8px 10px;
  font-size: 18px;

  &:focus {
    outline: none;
  }

  &:active {
    outline: none;
  }
`;

export const saveBtnStyle = {
  display: 'flex',
  justifyContent: 'flex-end',
  margin: '0 60px 0 0 ',
};

export const FieldWrapper = styled.div`
  position: relative;

  span[role='button'] {
    position: absolute;
    top: 15px;
    right: 65px;
    cursor: pointer;
  }
`;

export const TitlePrimary = styled.h2`
  font-size: 22px;
  color: #000;
  margin-bottom: 0;
`;

export const Wrapper = styled.div`
  padding: 50px 20px 12px 20px;
  background-color: #fff;
  height: 100%;
`;

export const SaveBtnWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  width: 100%;
  margin-top: 5px;
  padding: 0 10% 0 0;
`;
