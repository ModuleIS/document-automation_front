import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

export const Sidebar = styled.aside`
  width: 280px;
  height: 100%;
  padding-right: 8px;
`;

export const SidebarWrapper = styled.div`
  height: 100%;
  background-color: #fff;
  padding: 45px 20px;
  text-align: center;
`;

export const Link = styled(NavLink)`
  display: block;
  font-size: 22px;
  color: #898989;
  padding: 8px 0;
  border: solid 2px transparent;

  &.active {
    color: #000;
    border-radius: 10px;
    background-image: linear-gradient(white, white), linear-gradient(to right, #afffff, #faffa0);
    background-origin: border-box;
    background-clip: padding-box, border-box;
  }

  &:hover {
    color: #000;
  }
`;
