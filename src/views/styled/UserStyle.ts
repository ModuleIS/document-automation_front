import styled from 'styled-components';

export const ContainerSearch = styled.div`
  margin-top: 18px;
`;

export const FilterActivity = styled.div`
  display: flex;
  margin-top: 42px;
`;

export const DatePickerText = styled.div`
  font-size: 18px;
  color: #000;
`;

export const Documents = styled.div`
  margin-top: 35px;
`;

export const DocumentGroup = styled.div`
  ul {
    margin-top: 15px;
  }

  ul > li {
    margin-top: 10px;
  }
`;

export const DateText = styled.div`
  font-size: 18px;
  color: #000;
`;

export const DocumentLink = styled.a`
  font-size: 18px;
  color: #2833d0;
`;
