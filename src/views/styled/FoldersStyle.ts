import styled from 'styled-components';

export const Container = styled.div`
  margin-top: 22px;
`;

export const FolderFilter = styled.div`
  position: relative;
  width: 200px;
  margin-top: 5px;
`;

export const FilterIcon = styled.div`
  position: absolute;
  top: 6px;
  left: 0;
  cursor: pointer;
`;

export const Folders = styled.div`
  display: flex;
  margin: 22px 0 35px 0;
`;

export const Folder = styled.div`
  display: flex;
  align-items: center;
  min-width: 230px;
  margin-right: 25px;
  font-size: 18px;
  padding: 0 25px 0 30px;
  border: double 2px transparent;
  border-radius: 20px;
  background-image: linear-gradient(white, white), linear-gradient(to right, #afffff, #faffa0);
  background-origin: border-box;
  background-clip: padding-box, border-box;
`;

export const FolderIcon = styled.span`
  margin-left: auto;
  height: 36px;
  padding-left: 12px;
`;
