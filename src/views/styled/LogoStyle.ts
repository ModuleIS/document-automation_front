import styled from 'styled-components';

export const Logo = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 45px;
`;

export const Description = styled.span`
  font-family: 'Poppins', sans-serif;
  font-size: 24px;
  font-weight: 700;
  margin-left: 16px;

  @media (max-width: 450px) {
    width: 180px;
  }
`;

export const Image = styled.img`
  max-width: 100%;
  height: 100%;
  object-fit: cover;
`;
