import styled from 'styled-components';
import { Pagination } from 'antd';

export const PagePagination = styled(Pagination)`
  width: calc(100% - 30px);
  margin-top: auto;
  text-align: center;
  padding: 20px 0;

  & > li {
    min-width: 25px;
    height: 25px;
    line-height: 25px;
    font-weight: 600;
    border: 0;
    & > a {
      padding: 0;
    }
  }

  .ant-pagination-item-active {
    border-radius: 6px;
    background-color: #e8f063;
    transform: rotate(45deg);
    & > a {
      color: #000;
      transform: rotate(-45deg);
    }
  }

  .ant-pagination-item:hover {
    & > a {
      color: #9e9e9e;
    }
  }

  .ant-pagination-item:focus a {
    color: #000;
  }
`;
