import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  margin-top: 50px;
`;

export const Filters = styled.div`
  display: flex;
  margin-top: 60px;

  & > div:nth-child(1) {
    margin-right: 6%;
  }

  & > div:nth-child(2),
  & > div:nth-child(3) {
    margin-right: 3.5%;
  }

  & > div:nth-child(4) {
    margin-right: 4%;
  }
`;

export const UserWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const Text = styled.span<{ textDark: boolean }>`
  font-size: 18px;
  line-height: 1.5;
  color: ${({ textDark }) => (textDark ? '#000' : '#898989')};
  letter-spacing: .35px;
`;

export const SortIcon = styled.span<{ sort?: string }>`
  position: relative;
  top: 8px;
  width: auto;
  height: 20px;
  transform: rotate(${({ sort }) => (sort === 'asc' ? '180deg' : '0')});
  transform-origin: center;
`;

export const Actions = styled.div`
  display: flex;
  flex-direction: column;

  &:last-child button {
    margin-top: 15px;
  }
`;