import styled from 'styled-components';

export const Wrapper = styled.div`
  text-align: center;
  padding: 140px 15px 68px 15px;
  background-color: #f3f7f8;

  @media (max-width: 560px) {
    padding: 70px 0;
  }
`;

export const Title = styled.h1`
  font-family: 'Poppins', sans-serif;
  font-size: 52px;
  line-height: 1;
  font-weight: 400;
  color: #000;

  @media (max-width: 768px) {
    font-size: 36px;
    line-height: 1.2;
  }

  @media (max-width: 420px) {
    font-size: 28px;
    line-height: 1.35;
  }
`;
