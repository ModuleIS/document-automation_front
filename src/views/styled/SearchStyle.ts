import styled from 'styled-components';
import { Input } from 'antd';

const { Search } = Input;

export const SearchField = styled(Search)`
  &.ant-input-group-wrapper {
    max-width: 700px;
    width: 100%;
    margin-right: auto;
  }

  .ant-input {
    font-size: 18px;
  }

  .ant-input-affix-wrapper {
    border: double 2px transparent;
    background-image: linear-gradient(white, white), linear-gradient(to right, #afffff, #faffa0);
    background-origin: border-box;
    background-clip: padding-box, border-box;
  }

  .ant-input::placeholder {
    font-size: 20px;
    color: #898989;
  }

  .ant-input-affix-wrapper {
    padding: 4px 15px 4px 60px;
  }

  &.ant-input-search .ant-input-group .ant-input-affix-wrapper {
    border-radius: 20px;
  }

  &.ant-input-search > .ant-input-group > .ant-input-group-addon:last-child {
    top: 1px;
    left: auto;
    right: calc(100% - 58px);
  }

  &.ant-input-search
    > .ant-input-group
    > .ant-input-group-addon:last-child
    .ant-input-search-button:not(.ant-btn-primary) {
    color: #000;
  }

  .ant-input-group-addon {
    background-color: transparent;
    z-index: 2;
  }

  .ant-btn {
    border: 0;
    box-shadow: none;
    background: transparent;
  }

  .ant-btn-icon-only > * {
    font-size: 20px;
  }
`;
