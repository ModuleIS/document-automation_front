import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { Button } from 'antd';

export const Wrapper = styled.div`
  position: relative;
  max-width: 565px;
  margin: auto;
  padding: 45px 35px;

  form div:nth-child(4) {
    margin-bottom: 16px;
  }
`;

export const ForgotPassword = styled(Link)`
  display: block;
  font-size: 14px;
  color: #6e6669;
  text-decoration: underline;

  &:hover {
    color: #6e6669;
    opacity: 0.7;
  }
`;

export const FormButtonWrapper = styled.div`
  margin-top: 20px;

  @media (max-width: 560px) {
    margin-top: 35px;
    text-align: center;
  }
`;

export const FormButton = styled(Button)`
  height: 50px;
  font-size: 16px;
  font-weight: 700;
  color: #404040;
  background: #e8f063;
  border-color: #e8f063;
  border-radius: 12px;
  padding: 4px 52px;

  &:hover,
  &:focus {
    opacity: 0.7;
    color: inherit;
    background: #e8f063;
    border-color: #e8f063;
  }

  &:active {
    opacity: 0.4;
    color: inherit;
    background: #e8f063;
    border-color: #e8f063;
  }

  &:disabled {
    border: 0;
  }
`;
