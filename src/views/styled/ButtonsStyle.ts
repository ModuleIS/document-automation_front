import styled, { css } from 'styled-components';
import { Button, Radio } from 'antd';

export const Buttons = styled.div<{ between: boolean; direction: string }>`
  text-align: right;
  ${({ between, direction }) =>
    between &&
    css`
      display: flex;
      flex-direction: ${direction === 'column' ? 'column' : 'row'};
      justify-content: space-between;
      align-items: center;

      @media (max-width: 560px) {
        justify-content: center;
      }
    `}
`;

export const ButtonPrimary = styled(Button)<{ small: boolean }>`
  height: ${({ small }) => (small ? '38px' : '50px')};
  font-size: 15px;
  min-width: 0;
  font-weight: 700;
  color: #404040;
  background: #e8f063;
  border-color: #e8f063;
  border-radius: 12px;
  padding: 4px 16px;

  &:hover,
  &:focus {
    opacity: 0.7;
    color: #404040;
    background: #e8f063;
    border-color: #e8f063;
  }

  &:active {
    opacity: 0.4;
    color: inherit;
    background: #e8f063;
    border-color: #e8f063;
  }

  &:disabled {
    border: 0;
  }
`;

export const ButtonFilter = styled(ButtonPrimary)`
  background-color: #f1f1f1;
  border-color: #f1f1f1;

  &:hover,
  &:focus {
    opacity: 0.6;
    color: #404040;
    background: #f1f1f1;
    border-color: #f1f1f1;
  }

  &:active {
    opacity: 0.4;
    color: inherit;
    background: #f1f1f1;
    border-color: #f1f1f1;
  }
`;

export const ButtonGradient = styled(Radio.Button)`
  width: 400px;
  height: 36px;
  font-size: 20px;
  line-height: 34px;
  color: #000;
  text-align: center;
  border: 0;
  z-index: 0;

  &.ant-radio-button-wrapper:hover {
    color: #9a9a9a;
  }

  &.ant-radio-button-wrapper:first-child {
    border: 0;
  }

  &.ant-radio-button-wrapper-checked:not(.ant-radio-button-wrapper-disabled)::before {
    background-color: transparent;
  }

  &.ant-radio-button-wrapper:not(:first-child)::before {
    width: 100%;
    height: auto;
    padding: 0;
  }

  &.ant-radio-button-wrapper-checked:not(.ant-radio-button-wrapper-disabled) {
    color: #000;
    box-shadow: none;
  }

  &::before,
  &::after {
    content: '';
    position: absolute;
    top: -1px;
    right: 0;
    bottom: -1px;
    display: block;
    width: 100%;
    border: 2px solid transparent;
    box-sizing: border-box;
    border-radius: 5px;
    background-image: linear-gradient(white, white),
      linear-gradient(
        90deg,
        rgba(244, 246, 213, 1) 0%,
        rgba(239, 255, 173, 0.75) 24%,
        rgba(238, 244, 220, 1) 48%,
        rgba(235, 244, 224, 1) 64%,
        rgba(214, 241, 229, 1) 81%,
        rgba(215, 239, 242, 1) 100%
      );
    background-origin: border-box;
    background-clip: padding-box, border-box;
    z-index: -1;
  }

  &.ant-radio-button-wrapper-checked {
    &::before,
    &::after {
      background: linear-gradient(
        90deg,
        rgba(244, 246, 213, 1) 0%,
        rgba(239, 255, 173, 0.75) 24%,
        rgba(238, 244, 220, 1) 48%,
        rgba(235, 244, 224, 1) 64%,
        rgba(214, 241, 229, 1) 81%,
        rgba(215, 239, 242, 1) 100%
      );
      border: 0;
    }
  }

  &::before {
    border-right: 0;
    transform: skewX(-30deg);
    transform-origin: 0 0;
  }

  &::after {
    border-left: 0;
    transform: skewX(30deg);
    transform-origin: 0 0;
  }
`;
