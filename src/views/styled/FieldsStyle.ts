import styled from 'styled-components';
import { Form, Input, Select } from 'antd';

export const FieldGroup = styled.div`
  display: flex;

  .ant-form-item {
    min-width: 320px;
    margin-right: 95px;
  }
`;

export const FormItem = styled(Form.Item)`
  margin-bottom: 38px;

  .ant-input-affix-wrapper,
  .react-tel-input > input {
    font-size: 16px;
    border: 0;
    border-bottom: 2px solid #000;
    padding: 4px 12px 4px 0;
    width: 100%;
  }

  &.react-tel-input > input:focus {
    border-bottom: 2px solid #40a9ff;
  }

  &.ant-form-item-has-error .react-tel-input > input:focus {
    border-color: #ff4d4f;
    box-shadow: none;
  }

  &.react-tel-input > input {
    border-color: #ff4d4f;
    &::placeholder {
      color: #ff4d4f;
    }
  }

  .react-tel-input > input::placeholder {
    font-size: 18px;
    color: #6e6669;
    transition: color 0.3s;

    @media (max-width: 560px) {
      font-size: 18px;
    }
  }

  .ant-input-affix-wrapper,
  .react-tel-input > input {
    font-size: 16px;
    border: 0;
    border-bottom: 2px solid #000;
    padding: 4px 12px 4px 0;
  }

  &.ant-form-item-has-error .ant-input-affix-wrapper,
  &.ant-form-item-has-error .react-tel-input > input {
    border-color: #ff4d4f;
    &::placeholder {
      color: #ff4d4f;
    }
  }

  .react-tel-input > input:focus {
    border-bottom: 2px solid #40a9ff;
  }

  &.ant-form-item-has-error .react-tel-input > input:focus {
    border-color: #ff4d4f;
    box-shadow: none;
  }

  &.ant-form-item-has-error {
    animation: transform_error 0.2s;
  }

  .ant-form-item-explain {
    position: absolute;
    bottom: -25px;

    @media (max-width: 560px) {
      font-size: 12px;
    }
  }

  .ant-input-affix-wrapper .ant-input {
    border: 0;
  }

  .ant-input-affix-wrapper:focus,
  .ant-input-affix-wrapper-focused {
    border-bottom: 2px solid #40a9ff;
    box-shadow: none;
  }

  .ant-input::placeholder,
  .react-tel-input > input::placeholder {
    font-size: 18px;
    color: #6e6669;
    transition: color 0.3s;

    @media (max-width: 560px) {
      font-size: 18px;
    }
  }

  .ant-input-prefix {
    margin-right: 0px;
    position: absolute;
    top: 6px;
    right: 35px;
    opacity: 0.6;
    z-index: 50;
    cursor: pointer;
  }

  @keyframes transform_error {
    33% {
      transform: translateX(-7px);
    }
    66% {
      transform: translateX(7px);
    }
    100% {
      transform: translateX(0);
    }
  }
`;

export const FormInput = styled(Input)`
  font-size: 16px;
  border: 0;
  border-bottom: 2px solid #000;
  padding: 4px 12px 4px 0;

  &.ant-input:focus {
    box-shadow: none;
  }

  .ant-input:focus,
  .ant-input-focused {
    border-bottom: 2px solid #40a9ff;
  }

  .ant-form-item-has-error .ant-input:focus {
    border-color: #ff4d4f;
  }

  &.ant-input::placeholder {
    font-size: 18px;
    color: #6e6669;
    transition: color 0.3s;

    @media (max-width: 560px) {
      font-size: 18px;
    }
  }
`;

export const FieldSelect = styled(Select)<{ position?: string; border?: number }>`
  width: 100%;
  max-width: 320px;
  color: #6e6669;
  cursor: pointer;

  .ant-select-selection-placeholder {
    color: #6e6669;
    left: 0;
  }

  .ant-select-selector {
    font-size: 18px;
  }

  &.ant-select-single:not(.ant-select-customize-input) .ant-select-selector {
    height: 38px;
    cursor: pointer;
  }

  &.ant-select:not(.ant-select-customize-input) .ant-select-selector {
    text-align: ${({ position }) => position ?? 'left'};
    border: 0;
    border-bottom: ${({ border }) => border ?? '2px solid #000'};
    padding: 4px 12px 4px 0;
  }

  &.ant-select-focused:not(.ant-select-disabled).ant-select:not(.ant-select-customize-input)
    .ant-select-selector {
    box-shadow: none;
  }

  .ant-select-arrow-loading {
    font-size: 22px;
    width: 20px;
    height: 20px;
    margin-top: -12px;
  }
`;

export const FieldSelectGradient = styled(Select)`
  font-size: 18px;
  color: #898989;

  &.ant-select-single:not(.ant-select-customize-input) .ant-select-selector {
    width: 150px;
    height: 38px;
    border-radius: 10px;
  }

  &.ant-select:not(.ant-select-customize-input) .ant-select-selector {
    border: double 2px transparent;
    background-image: linear-gradient(white, white), linear-gradient(to right, #afffff, #faffa0);
    background-origin: border-box;
    background-clip: padding-box, border-box;
  }

  .ant-select-arrow-loading {
    font-size: 22px;
    width: 20px;
    height: 20px;
    margin-top: -12px;
  }

  .ant-select-selection-placeholder {
    color: #898989;
  }
`;
