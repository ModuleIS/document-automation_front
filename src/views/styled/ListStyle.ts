import styled from 'styled-components';

export const List = styled.div<{height?: number}>`
  display: flex;
  flex-direction: column;
  height: ${({height}) => height ? `calc(100% - ${height + 'px'})` : 'calc(100% - 358px)'};
  margin-top: 6px;
  padding: 0 25px;
  background-color: #fff;
  overflow: hidden;
`;

export const Table = styled.table`
  width: 100%;

  tr {
    border-bottom: double 2px transparent;
    background-image: linear-gradient(white, white), linear-gradient(to right, #afffff, #faffa0);
    background-origin: border-box;
    background-clip: padding-box, border-box;
    cursor: pointer;
  }

  td {
    padding: 15px 0;
  }
`;
