import styled from 'styled-components';
import { Button } from 'antd';

export const ActionIcon = styled.img`
  cursor: pointer;
  height: 42px;
  width: 42px;
  object-fit: cover;
  margin: -10px 0 -10px 30px;
  &:hover {
    opacity: 0.7;
  }
`;

export const rightColumnStyle = {
  display: 'inline',
  width: '21%',
  paddingRight: '62px',
  cursor: 'context-menu',
};

export const leftColumnStyle = {
  cursor: 'context-menu',
  width: '79%',
};
export const subTitle = {
  minWidth: '120px',
  display: 'flex',
  justifyContent: 'center',
};

export const ModalDeleteWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  width: 80%;
  margin: 0 auto;
`;

export const PositionTitle = styled.span`
  min-width: '270px';
  justify-content: 'center';
  display: 'flex';
`;

export const ActionButton = styled(Button)<{ backgroundColor?: string }>`
  height: '29px';
  font-size: 12px;
  min-width: 138px;
  font-weight: 700;
  color: #000000;
  background: ${({ backgroundColor }) => backgroundColor};
  border: none;
  border-radius: 12px;
  padding: 4px 32px;

  &:hover,
  &:focus {
    opacity: 0.7;
    color: #000000;
    background: ${({ backgroundColor }) => backgroundColor};
    border: none;
  }

  &:active {
    opacity: 0.4;
    color: #000000;
    background: ${({ backgroundColor }) => backgroundColor};
    border: none;
  }
`;
