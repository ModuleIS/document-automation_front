import styled from 'styled-components';

export const CategoryText = styled.div`
  font-size: 18px;
  line-height: 1.5;
  color: #898989;
  padding: 2px 15px;
  border-radius: 10px;
  border: double 2px transparent;
  background-image: linear-gradient(white, white), linear-gradient(to right, #afffff, #faffa0);
  background-origin: border-box;
  background-clip: padding-box, border-box;
`;
