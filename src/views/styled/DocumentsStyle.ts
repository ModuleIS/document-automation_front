import styled from 'styled-components';

export const DocumentActions = styled.div`
  text-align: center;
`;

export const CreateDocument = styled.span`
  display: inline-block;
  width: 45px;
  height: 45px;
  transition: opacity 0.15s;
  cursor: pointer;

  &:hover,
  &:focus {
    opacity: 0.8;
  }

  &:active {
    opacity: 0.6;
  }
`;

export const FilterDocuments = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 20px;
`;

export const FilterIcon = styled.span`
  position: relative;
  top: 2px;
  display: inline-block;
  margin-left: 10px;
  cursor: pointer;
`;
