import styled from 'styled-components';

export const FieldTypeLabel = styled.span`
  font-weight: normal;
  font-size: 22px;
  line-height: 32px;
  display: block;
  margin: 0 20px 0 0;
`;
export const FlexInlineContainer = styled.div`
  display: flex;
  justify-content: start;
  align-items: center;
`;

export const FilterWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 400px;
  margin: 20px 0 0 0;
`;

export const BtnWrapper = styled.div`
  margin: 40px 0 35px 0;
`;

export const TypesFields = styled.div`
  display: flex;
  flex-direction: column;
  height: calc(100% - 50%);
  margin-top: 6px;
  padding: 0 20px;
  background-color: #fff;
  overflow: auto;
`;

export const SaveTypes = styled.div`
  display: flex;
  justify-content: flex-end;
  width: 100%;
  margin-top: 25px;
`;

export const BottomPanel = styled.div`
  padding: 4px 20px;
  background-color: #fff;
  margin-top: 5px;
  height: calc(100% - 443px);
`;
