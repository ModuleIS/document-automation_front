import React from 'react';
import { Logo, Image, Description } from 'views/styled/LogoStyle';
import logo from 'img/logo.png';

const LogoBase = () => (
  <Logo>
    <Image src={logo} alt="ДО КОМБІНАТ “ПРОГРЕС”" />
    <Description>ДО КОМБІНАТ “ПРОГРЕС”</Description>
  </Logo>
);

export default LogoBase;
