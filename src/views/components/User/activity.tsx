import React from 'react';

import DatePicker from 'views/components/DatePicker';
import { SearchField } from 'views/styled/SearchStyle';
import {
  ContainerSearch,
  FilterActivity,
  DatePickerText,
  Documents,
  DocumentGroup,
  DateText,
  DocumentLink,
} from 'views/styled/UserStyle';

const Activity = () => {
  return (
    <>
      <ContainerSearch>
        <SearchField placeholder="Пошук" allowClear />
      </ContainerSearch>
      <FilterActivity>
        <DatePickerText>Відфільтрувати історію активності</DatePickerText>
        <DatePicker />
      </FilterActivity>
      <Documents>
        <DocumentGroup>
          <DateText>10.12.2020 12:00 Дата последней сессии</DateText>
          <ul>
            <li>
              <DocumentLink>Документ 1</DocumentLink>
            </li>
            <li>
              <DocumentLink>Документ 2</DocumentLink>
            </li>
          </ul>
        </DocumentGroup>
      </Documents>
    </>
  );
};

export default Activity;
