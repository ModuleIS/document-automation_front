import React, { useEffect } from 'react';
import { useHistory, useRouteMatch, useLocation } from 'react-router-dom';
import styled from 'styled-components';
import { Radio } from 'antd';

import Activity from './activity';
import Account from 'views/components/CreateUser';
import ButtonBack from 'views/UI/buttons/buttonBack';

import { Title, Head, TopPanel } from 'views/styled/DashboardStyle';
import { ButtonPrimary, ButtonGradient } from 'views/styled/ButtonsStyle';

const Wrapper = styled.div`
  height: calc(100% - 225px);
  margin-top: 6px;
  padding: 0 20px;
  background-color: #fff;
  overflow: auto;
`;

const Tabs = styled(Radio.Group)`
  margin: 35px 0 0 20px;

  label:first-child {
    margin-right: 60px;
  }
`;

const User = () => {
  const { push } = useHistory();
  const { url } = useRouteMatch();
  const { search } = useLocation();
  const params = new URLSearchParams(search);

  useEffect(() => {
    push({ pathname: url, search: '?status=activity' });
  }, []);

  const handleClickToBack = (): void => {
    push('/users');
  };

  const handleClickSwitch = (search: string): void => {
    push({ pathname: url, search });
  };

  return (
    <>
      <TopPanel>
        <Head bottom={null} isShow>
          <ButtonBack onClick={handleClickToBack} />
          <Title>Іванов Іван Миколайович</Title>
        </Head>
        <ButtonPrimary small>Деактивувати користувача</ButtonPrimary>
        <Tabs defaultValue="activity">
          <ButtonGradient value="activity" onClick={() => handleClickSwitch('?status=activity')}>
            Активність
          </ButtonGradient>
          <ButtonGradient value="account" onClick={() => handleClickSwitch('?status=account')}>
            Обліковий запис
          </ButtonGradient>
        </Tabs>
      </TopPanel>
      <Wrapper>{params.get('status') === 'activity' ? <Activity /> : <Account />}</Wrapper>
    </>
  );
};

export default User;
