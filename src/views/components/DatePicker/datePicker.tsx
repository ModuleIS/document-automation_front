import React from 'react';
import { DatePicker } from 'antd';
import { DatePickerField } from 'views/styled/DatePickerStyle';

const { RangePicker } = DatePicker;

const Datepicker = () => {
  return (
    <DatePickerField>
      <RangePicker
        showTime
        separator=""
        placeholder={['дата початку', 'дата закінчення']}
        format="DD/MM/YYYY HH:mm"
        autoFocus
      />
    </DatePickerField>
  );
};

export default Datepicker;
