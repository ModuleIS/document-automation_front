import React from 'react';

import { convert } from 'utilities/helpers/convert';
import getHighlight from 'utilities/helpers/highlightFound';
import { Text } from 'views/styled/UsersStyle';

interface IUser {
  firstname: string;
  secondname: string;
  patronymic: string;
  role: string;
  position: {
    id: number;
    name: string;
  };
  status: string;
  creationdate: string;
  findText: string,
  onClick: () => void;
}

const User = ({
  patronymic,
  firstname,
  secondname,
  role,
  position,
  status,
  creationdate,
  findText,
  onClick,
}: IUser) => {
  return (
    <tr onClick={onClick}>
      <td style={{ width: '30%' }}>
        <Text textDark>
          <span>{secondname}&nbsp;</span>
          <span>
            { findText && <mark>{ findText }</mark> }
            {getHighlight(findText, firstname)}&nbsp;
          </span>
          <span>{patronymic}&nbsp;</span>
        </Text>
      </td>
      <td style={{ width: '18%' }}>
        <Text textDark={false}>{convert(role, 'roles')}</Text>
      </td>
      <td style={{ width: '18%' }}>
        <Text textDark={false}>{position.name}</Text>
      </td>
      <td style={{ width: '18%' }}>
        <Text textDark={false} style={{width: '150px'}}>{convert(status, 'statuses')}</Text>
      </td>
      <td style={{ width: '20%' }}>
        <Text textDark={false}>{new Date(creationdate).toLocaleDateString()}</Text>
      </td>
    </tr>
  );
};

export default User;
