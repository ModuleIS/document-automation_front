import React, { useCallback, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Select, Empty } from 'antd';

import Item from './item';
import Indicator from 'views/components/Indicator';
import Pagination from 'views/components/Pagination';

import { Wrapper, Filters, Actions } from 'views/styled/UsersStyle';
import { ButtonPrimary, ButtonFilter } from 'views/styled/ButtonsStyle';
import { Title, TopPanel } from 'views/styled/DashboardStyle';
import { FieldSelectGradient } from 'views/styled/FieldsStyle';
import { List, Table } from 'views/styled/ListStyle';
import { CategoryText } from 'views/styled/CategoryStyle';
import { SearchField } from 'views/styled/SearchStyle';

import { usersAsync, filterUsersAsync } from 'states/ducks/users/action';
import { positionsAsync } from 'states/ducks/positions/action';
import { getComputedPositions } from 'states/ducks/positions/selectors';
import { useFilter } from 'utilities/hooks/useFilter';
import { useSearch } from 'utilities/hooks/useSearch';
import { RootState } from 'states/store';

const { Option } = Select;

const Users = () => {
  const dispatch = useDispatch();

  const getUsers = useCallback((queryParams?) => dispatch(usersAsync(queryParams)), [dispatch]);
  const getPositions = useCallback(() => dispatch(positionsAsync()), [dispatch]);
  const getFilter = useCallback((params) => dispatch(filterUsersAsync(params)), [dispatch]);

  const { loading, users, totalPage } = useSelector((state: RootState) => state.users);
  const { positions, loadingPositions } = useSelector(getComputedPositions);

  const { push } = useHistory();
  const { values, isDisabled, onFilters, onClearFilter } = useFilter({
    role: undefined,
    position: undefined,
    status: undefined
  }, getFilter);
  const { value, onSearch } = useSearch(getFilter);

  const isEmpty = (
    <tr style={{ border: 0 }}>
      <th>
        <Empty description="Нічого не знайдено" image={Empty.PRESENTED_IMAGE_SIMPLE} />
      </th>
    </tr>
  );

  const indicator = (
    <tr style={{ border: 0 }}>
      <th>
        <Indicator position="center" />
      </th>
    </tr>
  );

  useEffect(() => {
    getUsers();
    getPositions();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleClickCreate = (): void => {
    push('/users/create');
  };

  const handleClickCurrentUser = (): void => {
    push(`/users/${1}`);
  };

  return (
    <>
      <TopPanel>
        <Title>Користувачі</Title>
        <Wrapper>
          <SearchField
            placeholder="Пошук" 
            allowClear
            value={ value }
            onChange={ onSearch }
          />
          <Actions>
            <ButtonPrimary 
              small 
              onClick={handleClickCreate}
            >
              Додати користувача
            </ButtonPrimary>
            <ButtonFilter
              small
              onClick={ onClearFilter }
              disabled={ !!isDisabled }
            >
              Очистити фільтр
            </ButtonFilter>
          </Actions>
        </Wrapper>
        <Filters>
          <div>
            <CategoryText>Прізвище ім’я по батькові</CategoryText>
          </div>
          <div>
            <FieldSelectGradient
              placeholder="Роль"
              value={ values.role }
              onChange={(value) => onFilters(value, 'role')}
            >
              <Option value="ADMIN">Адміністратор</Option>
              <Option value="USER">Користувач</Option>
            </FieldSelectGradient>
          </div>
          <div>
            <FieldSelectGradient
              placeholder='Посада'
              value={ values.position }
              loading={ loadingPositions }
              onChange={ (value) => onFilters(value, 'position') }
            >
              {positions.map(({ value, label, id }: any) => (
                <Option key={id} value={value}>
                  {label}
                </Option>
              ))}
            </FieldSelectGradient>
          </div>
          <div>
            <FieldSelectGradient
              placeholder='Статус'
              value={ values.status }
              onChange={ (value) => onFilters(value, 'status') }
            >
              <Option value="INVITATION_SENT">Надіслано запрошення</Option>
              <Option value="ACTIVE">Онлайн</Option>
              <Option value="DEACTIVATED">Деактивовано</Option>
              <Option value="PASSWORD_RESTORE">Відновлення паролю</Option>
            </FieldSelectGradient>
          </div>
          <div>
            <CategoryText>Дата реєстрації</CategoryText>
          </div>
          {/* <SortIcon
            onClick={ handleSortUsers }
            sort={sort}
          >
            <svg 
              xmlns="http://www.w3.org/2000/svg" 
              width='20' 
              height='20' 
              viewBox="0 0 21.825 21.825"
            >
              <path d="M16.791 13.254a1.112 1.112 0 011.587 0 1.14 1.14 0 010 1.587l-6.65 6.651a1.14 1.14 0 01-.809.333c-.317 0-.603-.127-.81-.333l-6.65-6.651c-.444-.444-.444-1.143 0-1.587s1.143-.444 1.587 0l4.746 4.762V1.111A1.116 1.116 0 0110.918 0c.619 0 1.111.492 1.111 1.111v16.904l4.762-4.761z" fill="#898990"/>
            </svg>
          </SortIcon> */}
        </Filters>
      </TopPanel>
      <List>
        <Table>
          <tbody>
            {!loading && !users.length && isEmpty}
            {!!loading
              ? indicator
              : users.map(({ id, ...otherParams }: any) => (
                  <Item 
                    key={id} 
                    {...otherParams}
                    findText={ value }
                    onClick={handleClickCurrentUser}
                  />
                ))}
          </tbody>
        </Table>
        <Pagination 
          totalPage={totalPage} 
          onGetUsers={getUsers} 
        />
      </List>
    </>
  );
};

export default Users;
