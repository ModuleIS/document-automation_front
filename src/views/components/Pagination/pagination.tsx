import React, { useEffect } from 'react';
import { useHistory, useLocation, useRouteMatch } from 'react-router-dom';

import { PagePagination } from 'views/styled/PaginationStyle';

interface IPagination {
  totalPage: number;
  onGetUsers: (query: string) => void;
}

const PaginationBase = ({ totalPage, onGetUsers }: IPagination) => {
  const { push } = useHistory();
  const { url } = useRouteMatch();
  const { search } = useLocation();
  const params = new URLSearchParams(search);

  useEffect(() => {
    if (totalPage > 5) {
      push({pathname: url, search: '?page=1'});
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [totalPage]);

  const handleChange = (page: number): void => {
    onGetUsers(`?page=${page-1}&size=5`);
    push({pathname: url, search: `?page=${page}`});
  }

  const itemRender = (_: number, type: string, originalElement: React.ReactElement<HTMLElement>): React.ReactElement<HTMLElement> | null => {
    if (type === 'prev') {
      return null;
    }
    if (type === 'next') {
      return null;
    }

    return originalElement;
  };

  return (
    <PagePagination
      total={totalPage}
      current={parseInt(params.get('page')!)}
      hideOnSinglePage
      defaultPageSize={ 5 }
      onChange={ handleChange }
      itemRender={ itemRender }
    />
  );
};

export default PaginationBase;
