import React from 'react';

import { Text } from 'views/styled/UsersStyle';

const Document = ({ onClick }: {onClick: () => void}) => (
  <tr onClick={ onClick }>
    <td style={{width: '23%'}}>
      <Text textDark>Test</Text>
    </td>
    <td style={{width: '17.5%'}}>
      <Text textDark={false}>pdf</Text>
    </td>
    <td style={{width: '17.5%'}}>
      <Text textDark={false}>Test</Text>
    </td>
    <td style={{width: '24%'}}>
      <Text textDark={false}>23/12/2020</Text>
    </td>
    <td>
      <Text textDark={false}>23/12/2020</Text>
    </td>
  </tr>
);

export default Document;