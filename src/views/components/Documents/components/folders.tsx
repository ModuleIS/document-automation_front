import React from 'react';
import { Select } from 'antd';

import { FieldSelect } from 'views/styled/FieldsStyle';
import {
  FolderFilter,
  FilterIcon,
  Folders,
  Folder,
  FolderIcon,
  Container,
} from 'views/styled/FoldersStyle';
import { Title } from 'views/styled/DashboardStyle';

const { Option } = Select;

const FoldersBase = () => {
  return (
    <Container>
      <Title>Теки</Title>
      <FolderFilter>
        <FieldSelect 
          placeholder="Назва" 
          position="center" 
          border={0}
        >
          <Option value='NAME'>Назва</Option>
          <Option value='RECENT_CHANGES'>Останні зміни</Option>
        </FieldSelect>
        <FilterIcon role="button">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="22"
            height="22"
            viewBox="0 0 21.825 21.825"
          >
            <path
              d="M16.791 13.254a1.112 1.112 0 011.587 0 1.14 1.14 0 010 1.587l-6.65 6.651a1.14 1.14 0 01-.809.333c-.317 0-.603-.127-.81-.333l-6.65-6.651c-.444-.444-.444-1.143 0-1.587s1.143-.444 1.587 0l4.746 4.762V1.111A1.116 1.116 0 0110.918 0c.619 0 1.111.492 1.111 1.111v16.904l4.762-4.761z"
              fill="#898990"
            />
          </svg>
        </FilterIcon>
      </FolderFilter>
      <Folders>
        <Folder>
          Вхідні документи
          <FolderIcon>
            <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 512 512">
              <path
                xmlns="http://www.w3.org/2000/svg"
                fill="#73bd55"
                data-original="#e0a02f"
                d="M0 118.904h512v-43.2H194.848l-25.664-25.392H30.72L0 81.032z"
              />
              <path
                xmlns="http://www.w3.org/2000/svg"
                fill="#73bd55"
                data-original="#ffcc67"
                d="M0 118.904h512v342.768H0z"
              />
              <path
                fill="#73bd55"
                data-original="#e0a02f"
                d="M0 449.112h512v12.576H0z"
                xmlns="http://www.w3.org/2000/svg"
                opacity=".5"
              />
              <path
                xmlns="http://www.w3.org/2000/svg"
                fill="#fff"
                data-original="#ffffff"
                d="M20.704 97.32h470.592v21.584H20.704z"
              />
            </svg>
          </FolderIcon>
        </Folder>
        <Folder>
          Внутрішні документи
          <FolderIcon>
            <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 512 512">
              <path
                xmlns="http://www.w3.org/2000/svg"
                fill="#73bd55"
                data-original="#e0a02f"
                d="M0 118.904h512v-43.2H194.848l-25.664-25.392H30.72L0 81.032z"
              />
              <path
                xmlns="http://www.w3.org/2000/svg"
                fill="#73bd55"
                data-original="#ffcc67"
                d="M0 118.904h512v342.768H0z"
              />
              <path
                fill="#73bd55"
                data-original="#e0a02f"
                d="M0 449.112h512v12.576H0z"
                xmlns="http://www.w3.org/2000/svg"
                opacity=".5"
              />
              <path
                xmlns="http://www.w3.org/2000/svg"
                fill="#fff"
                data-original="#ffffff"
                d="M20.704 97.32h470.592v21.584H20.704z"
              />
            </svg>
          </FolderIcon>
        </Folder>
        <Folder>
          Вихідні документи
          <FolderIcon>
            <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 512 512">
              <path
                xmlns="http://www.w3.org/2000/svg"
                fill="#73bd55"
                data-original="#e0a02f"
                d="M0 118.904h512v-43.2H194.848l-25.664-25.392H30.72L0 81.032z"
              />
              <path
                xmlns="http://www.w3.org/2000/svg"
                fill="#73bd55"
                data-original="#ffcc67"
                d="M0 118.904h512v342.768H0z"
              />
              <path
                fill="#73bd55"
                data-original="#e0a02f"
                d="M0 449.112h512v12.576H0z"
                xmlns="http://www.w3.org/2000/svg"
                opacity=".5"
              />
              <path
                xmlns="http://www.w3.org/2000/svg"
                fill="#fff"
                data-original="#ffffff"
                d="M20.704 97.32h470.592v21.584H20.704z"
              />
            </svg>
          </FolderIcon>
        </Folder>
      </Folders>
    </Container>
  );
};

export default FoldersBase;
