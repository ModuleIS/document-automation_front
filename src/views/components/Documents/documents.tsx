import React from 'react';
import { Select } from 'antd';

import Folders from './components/folders';

import Document from './components/document';

import { TopPanel, Title } from 'views/styled/DashboardStyle';
import { DocumentActions, CreateDocument, FilterDocuments, FilterIcon } from 'views/styled/DocumentsStyle';
import { FieldSelectGradient } from 'views/styled/FieldsStyle';
import { CategoryText } from 'views/styled/CategoryStyle';
import { List, Table } from 'views/styled/ListStyle';
import { SearchField } from 'views/styled/SearchStyle';
import img from 'img/createDocument.png';

const { Option } = Select;

const Documents = () => {
  const handleClick = (): void => {
    window.open('http://www.africau.edu/images/default/sample.pdf', '_black');
  }

  return (
    <>
      <TopPanel>
        <DocumentActions>
          <SearchField
            placeholder="Пошук" 
            allowClear
          />
          <CreateDocument 
            role='button'
          >
            <img src={img} alt="Додаты документ"/>
          </CreateDocument>
        </DocumentActions>
        <Folders />
        <Title>Документи</Title>
        <FilterDocuments>
          <CategoryText>
            Назва документу
            <FilterIcon role="button">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                viewBox="0 0 21.825 21.825"
              >
                <path
                  d="M16.791 13.254a1.112 1.112 0 011.587 0 1.14 1.14 0 010 1.587l-6.65 6.651a1.14 1.14 0 01-.809.333c-.317 0-.603-.127-.81-.333l-6.65-6.651c-.444-.444-.444-1.143 0-1.587s1.143-.444 1.587 0l4.746 4.762V1.111A1.116 1.116 0 0110.918 0c.619 0 1.111.492 1.111 1.111v16.904l4.762-4.761z"
                  fill="#898990"
                />
              </svg>
            </FilterIcon>
          </CategoryText>
          <FieldSelectGradient 
            placeholder="Формат" 
          >
            <Option value='PDF'>pdf</Option>
          </FieldSelectGradient>
          <FieldSelectGradient placeholder="Тип документу" />
          <CategoryText>
            Дата останніх змін
            <FilterIcon role="button">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                viewBox="0 0 21.825 21.825"
              >
                <path
                  d="M16.791 13.254a1.112 1.112 0 011.587 0 1.14 1.14 0 010 1.587l-6.65 6.651a1.14 1.14 0 01-.809.333c-.317 0-.603-.127-.81-.333l-6.65-6.651c-.444-.444-.444-1.143 0-1.587s1.143-.444 1.587 0l4.746 4.762V1.111A1.116 1.116 0 0110.918 0c.619 0 1.111.492 1.111 1.111v16.904l4.762-4.761z"
                  fill="#898990"
                />
              </svg>
            </FilterIcon>
          </CategoryText>
          <CategoryText>
            Дата створення
            <FilterIcon role="button">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                viewBox="0 0 21.825 21.825"
              >
                <path
                  d="M16.791 13.254a1.112 1.112 0 011.587 0 1.14 1.14 0 010 1.587l-6.65 6.651a1.14 1.14 0 01-.809.333c-.317 0-.603-.127-.81-.333l-6.65-6.651c-.444-.444-.444-1.143 0-1.587s1.143-.444 1.587 0l4.746 4.762V1.111A1.116 1.116 0 0110.918 0c.619 0 1.111.492 1.111 1.111v16.904l4.762-4.761z"
                  fill="#898990"
                />
              </svg>
            </FilterIcon>
          </CategoryText>
        </FilterDocuments>
      </TopPanel>
      <List
        height={412}
      >
        <Table>
          <tbody>
            <Document
              onClick={ handleClick }
            />
          </tbody>
        </Table>
      </List>
    </>
  );
};

export default Documents;
