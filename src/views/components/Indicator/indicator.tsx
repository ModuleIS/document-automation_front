import React from 'react';
import styled from 'styled-components';
import { Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';

const Loader = styled.div`
  padding: 25px 0;

  .anticon-loading svg {
    color: #000;
  }
`;

interface IIndicator {
  position?: any;
  isText?: boolean;
}

const Indicator = ({ position, isText }: IIndicator) => {
  const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

  return (
    <Loader style={{ textAlign: position }}>
      <Spin indicator={antIcon} />
      {!isText ? null : <div>Loading</div>}
    </Loader>
  );
};

export default Indicator;
