import React from 'react';

import Logo from 'views/components/Logo';
import { Wrapper, Title } from 'views/styled/IntroStyle';

const Intro = () => (
  <Wrapper>
    <Logo />
    <Title>Вітаємо Вас в системі документообігу</Title>
  </Wrapper>
);

export default Intro;
