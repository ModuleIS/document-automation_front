import React, { MutableRefObject, useCallback, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Wrapper, Filters } from 'views/styled/UsersStyle';
import Indicator from 'views/components/Indicator';
import { RootState } from 'states/store';
import PositionItem from './components/position-item';
import { ButtonPrimary } from 'views/styled/ButtonsStyle';
import { SearchField } from 'views/styled/SearchStyle';
import { Title, TopPanel } from 'views/styled/DashboardStyle';
import { FormInput } from 'views/styled/FieldsStyle';
import { CategoryText } from 'views/styled/CategoryStyle';
import { List, Table } from 'views/styled/ListStyle';

import { Modal } from 'antd';
import { ActionButton, ModalDeleteWrapper, subTitle } from 'views/styled/PositionStyle';
import {
  createPositionAsync,
  deletePositionAsync,
  getPositionsWithPagination,
  setPositionsError,
  updatePositionAsync,
} from 'states/ducks/positions/action';
import { config } from 'utilities/helpers/configDialog';
import Pagination from '../Pagination';

const UsersPositions = () => {
  const dispatch = useDispatch();

  const getPositions = useCallback((params?) => dispatch(getPositionsWithPagination(params)), [
    dispatch,
  ]);
  const setError = useCallback((err) => dispatch(setPositionsError(err)), [dispatch]);
  const deletePosition = useCallback((id) => dispatch(deletePositionAsync(id)), [dispatch]);
  const createPosition = useCallback((position) => dispatch(createPositionAsync(position)), [
    dispatch,
  ]);
  const updatePosition = useCallback((position) => dispatch(updatePositionAsync(position)), [
    dispatch,
  ]);

  const inputRef: MutableRefObject<any> = useRef(null);
  useEffect(() => {
    getPositions();
  }, [getPositions]);

  const { loadingPositions, positionsFiltered, totalPage, errorPositions } = useSelector(
    (state: RootState) => state.positions,
  );

  useEffect(() => {
    if (errorPositions === '423') {
      openDeletePositionError();
      setError(null);
    }
  }, [errorPositions]);

  const handleClickSavePosition = (modalClose: () => void): void => {
    const { value } = inputRef.current.input;
    if (value) {
      createPosition({ name: value });
      modalClose();
    }
  };

  const handleClickOpenSetPosition = (): void => {
    const modal = Modal.info({
      ...config,
      title: 'Додати посаду',
      bodyStyle: { textAlign: 'right' },
      content: (
        <>
          <FormInput placeholder="Назва посади" ref={inputRef} />
          <ButtonPrimary small onClick={() => handleClickSavePosition(modal.destroy)}>
            Додати посаду
          </ButtonPrimary>
        </>
      ),
    });
  };

  const handleClickDeletePosition = (modalClose: () => void, id: number): void => {
    if (id) {
      deletePosition(id);
      modalClose();
    }
  };

  const openDeletePosition = (id: number): void => {
    const modal = Modal.info({
      ...config,
      title: 'Ви впевнені, що хочете видалити посаду?',
      bodyStyle: { textAlign: 'right' },
      content: (
        <>
          <ModalDeleteWrapper>
            <ActionButton backgroundColor={'#F1ACAC'} onClick={() => modal.destroy()}>
              Скасувати
            </ActionButton>

            <ActionButton
              onClick={() => handleClickDeletePosition(modal.destroy, id)}
              backgroundColor={'#C2FFBF'}
            >
              Видалити
            </ActionButton>
          </ModalDeleteWrapper>
        </>
      ),
    });
  };
  const openDeletePositionError = (): void => {
    Modal.info({
      ...config,
      title: 'Ви не можете видалити цю посаду тому, що вона закріплена за користувачем.',
      bodyStyle: { textAlign: 'right' },
    });
  };

  const handleClickEditPosition = (modalClose: () => void, id: number): void => {
    const { value } = inputRef.current.input;
    if (value.length) {
      const newPosition = {
        id: id,
        name: value,
      };
      updatePosition(newPosition);
      modalClose();
    }
  };

  const openEditPosition = (id: number): void => {
    // @ts-ignore
    const prevPositionValue = positionsFiltered.find((item) => item.id === id).name;

    const modal = Modal.info({
      ...config,
      title: 'Редагувати посаду',
      bodyStyle: { textAlign: 'right' },
      content: (
        <>
          <FormInput placeholder="Назва посади" defaultValue={prevPositionValue} ref={inputRef} />
          <ButtonPrimary small onClick={() => handleClickEditPosition(modal.destroy, id)}>
            Зберегти зміни
          </ButtonPrimary>
        </>
      ),
    });
  };

  return (
    <>
      <TopPanel>
        <Title>Посади</Title>
        <Wrapper>
          <SearchField placeholder="Пошук" allowClear />

          <ButtonPrimary small onClick={handleClickOpenSetPosition}>
            Додати посаду
          </ButtonPrimary>
        </Wrapper>
        <Filters>
          <CategoryText>
            <span style={subTitle}>Посада</span>
          </CategoryText>
        </Filters>
      </TopPanel>
      <List
        height={290}
      >
        <Table>
          {!!loadingPositions ? (
            <Indicator position="center" />
          ) : (
            positionsFiltered.map(({ name, id }) => (
              <PositionItem
                key={id}
                id={id}
                position={name}
                deleteModal={openDeletePosition}
                editModal={openEditPosition}
              />
            ))
          )}
        </Table>
        <Pagination totalPage={totalPage} onGetUsers={getPositions} />
      </List>
    </>
  );
};

export default UsersPositions;
