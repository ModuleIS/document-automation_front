import React from 'react';
import edit from 'img/edit_icon.svg';
import remove from 'img/delete_icon.svg';
import { UserWrapper, Text } from 'views/styled/UsersStyle';
import { ActionIcon, leftColumnStyle, rightColumnStyle } from 'views/styled/PositionStyle';
import { Table } from 'views/styled/ListStyle';
import { Tooltip } from 'antd';

interface IPosition {
  position: string;
  id: number;
  deleteModal: (id: number) => void;
  editModal: (id: number) => void;
}

const PositionItem = ({ position, deleteModal, editModal, id }: IPosition) => {
  return (
    <UserWrapper>
      <Table>
        <tbody>
          <tr>
            <td style={leftColumnStyle}>
              <Text textDark>{position}</Text>
            </td>
            <td style={rightColumnStyle}>
              <Tooltip placement="top" title="Редагувати">
                <ActionIcon src={edit} alt="edit" onClick={() => editModal(id)} />
              </Tooltip>

              <Tooltip placement="top" title="Видалити">
                <ActionIcon src={remove} alt="delete" onClick={() => deleteModal(id)} />
              </Tooltip>
            </td>
          </tr>
        </tbody>
      </Table>
    </UserWrapper>
  );
};
export default PositionItem;
