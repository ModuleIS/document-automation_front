import React from 'react';
import { useSelector } from 'react-redux';

import { RootState } from 'states/store';
import { SelectValue } from 'antd/lib/select';

import Indicator from '../Indicator';
import SimpleSelect from '../CreateAccessGroup/components/simpleSelect';
import TemplateMenu from './components/templateMenu';
import TypesItem from './components/typesItem';

import { Title, TopPanel } from 'views/styled/DashboardStyle';
import { Modal, Select } from 'antd';
import { config } from 'utilities/helpers/configDialog';
import { ActionButton, ModalDeleteWrapper } from 'views/styled/PositionStyle';
import { FieldSelectGradient } from 'views/styled/FieldsStyle';
import { FieldContainer, InputAccess } from '../CreateAccessGroup/accessGroupOptions';
import { ButtonPrimary } from 'views/styled/ButtonsStyle';
import { BtnPadding } from '../CreateAccessGroup/createAccessGroup';

import { CategoryText } from 'views/styled/CategoryStyle'
import {
  BottomPanel,
  BtnWrapper,
  FieldTypeLabel,
  FilterWrapper,
  FlexInlineContainer,
  SaveTypes,
} from 'views/styled/DocumentsType';

const { Option } = Select;

const Data = [
  { name: 'Заява', id: 2212, type: 'pdf' },
  { name: 'Акт', id: 232, type: 'doc' },
];

const fieldCard = [
  { name: 'Дата', id: 377 },
  { name: 'Підпис', id: 3234 },
  { name: 'Назва документу', id: 3247 },
  { name: 'Керівник', id: 3717 },
  { name: 'Вид наказу', id: 3741 },
  { name: 'Групи доступу', id: 37 },
];

const CreateDocumentsType: React.FC<{ match: { params: { id: string | undefined } } }> = ({
  match: {
    params: { id },
  },
}) => {
  const openEditDocumentType = (id: number) => {};

  const handleClickDeleteDocumentsType = (modalClose: () => void, id: number): void => {
    if (id) {
      // deleteDocumentType(id);
      modalClose();
    }
  };

  const openDeleteDocumentType = (id: number): void => {
    const modal = Modal.info({
      ...config,
      title: 'Ви впевнені, що хочете видалити тип документа?',
      bodyStyle: { textAlign: 'center' },
      content: (
        <>
          <ModalDeleteWrapper>
            <ActionButton backgroundColor={'#F1ACAC'} onClick={() => modal.destroy()}>
              Скасувати
            </ActionButton>

            <ActionButton
              onClick={() => handleClickDeleteDocumentsType(modal.destroy, id)}
              backgroundColor={'#C2FFBF'}
            >
              Видалити
            </ActionButton>
          </ModalDeleteWrapper>
        </>
      ),
    });
  };

  const { loading, documentsTypes, error } = useSelector(
    (state: RootState) => state.documentsTypes,
  );
  const handleSubmit = () => {
    // if(id){
    //   updateAccessGroup(initialValues)
    // } else createAccessGroup(initialValues)
    //
  };

  const handleFilter = (value: SelectValue, type: string) => {
    // if(id){
    //   updateAccessGroup(initialValues)
    // } else createAccessGroup(initialValues)
    //
  };

  return (
    <>
      <TopPanel>
        <Title>Додати тип документу доступу</Title>

        <FieldContainer>
          <InputAccess
            name="name"
            // onChange={setValue}
            // value={accessGroup.name}
            placeholder="Назва типу"
          />
        </FieldContainer>

        <FlexInlineContainer>
          <FieldTypeLabel>Поля картки :</FieldTypeLabel>
          <FieldContainer>
            <SimpleSelect options={fieldCard} placeholder="Назва" defaultValue={'Назва'} />
          </FieldContainer>
        </FlexInlineContainer>

        <BtnWrapper>
          <ButtonPrimary small onClick={() => void 0}>
            <BtnPadding> Створити маршрут для типу </BtnPadding>
          </ButtonPrimary>
        </BtnWrapper>

        <FlexInlineContainer>
          <FieldTypeLabel>Шаблони</FieldTypeLabel>

          <TemplateMenu />
        </FlexInlineContainer>

        <FilterWrapper>
          <CategoryText>Назва шаблону</CategoryText>

          <FieldSelectGradient
            placeholder="Формат"
            onChange={(value) => handleFilter(value, 'type')}
          >
            <Option value="DOC"> doc </Option>
            <Option value="EXEL"> exel </Option>
            <Option value="PDF">pdf</Option>
          </FieldSelectGradient>
        </FilterWrapper>
      </TopPanel>

      <BottomPanel>
        {!!loading ? (
          <Indicator position="center" />
        ) : (
          Data.map(({ name, id, type }) => (
            <TypesItem
              format={type}
              key={id}
              id={id}
              position={name}
              deleteModal={openDeleteDocumentType}
              editModal={openEditDocumentType}
            />
          ))
        )}
        <SaveTypes>
          <ButtonPrimary onClick={handleSubmit} small>
            <BtnPadding>Зберегти</BtnPadding>
          </ButtonPrimary>
        </SaveTypes>
      </BottomPanel>
    </>
  );
};

export default CreateDocumentsType;
