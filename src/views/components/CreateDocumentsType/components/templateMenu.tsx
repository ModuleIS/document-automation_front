import React from 'react';

import { Menu, Dropdown } from 'antd';
import styled from 'styled-components';
import arrow from 'img/select_arrow.svg';

export const DropdownHead = styled(Dropdown)`
  width: 220px;
  height: 31px;
  font-size: 15px;
  line-height: 18px;
  display: flex;
  justify-content: center;
  align-items: center;
  min-width: 0;
  font-weight: 700;
  color: #404040;
  background: #e8f063;
  border-color: #000000;
  border-radius: 12px;
  padding: 4px 12px 4px 28px;

  &:hover,
  &:focus {
    opacity: 0.7;
    color: #404040;
    background: #e8f063;
    border-color: #e8f063;
  }

  &:active {
    opacity: 0.4;
    color: inherit;
    background: #e8f063;
    border-color: #e8f063;
  }

  &:disabled {
    border: 0;
  }
`;

export const SelectIcon = styled.img`
  cursor: pointer;
  width: 16px;
  height: 7px;
  object-fit: cover;
  margin: 3px 0 0 13px;
`;

export const MenuItemContainer = styled.div`
  display: flex;
  background: #feffec;
  justify-content: center;
  align-items: center;
  height: 35px;
  border: double 1px transparent;
  font-size: 15px;
  line-height: 18px;
  background-image: linear-gradient(white, white), linear-gradient(to right, #afffff, #faffa0);
  background-origin: border-box;
  background-clip: padding-box, border-box;
  cursor: pointer;
  &:hover,
  &:focus {
    background: #e6f40052;
  }
`;

const TemplateMenu = () => {
  const menu = (
    <Menu>
      <MenuItemContainer key="0">Завантажити з ПК</MenuItemContainer>
      <MenuItemContainer key="1">Додати з документів</MenuItemContainer>
      <MenuItemContainer key="3">Створити новий</MenuItemContainer>
    </Menu>
  );

  return (
    <DropdownHead overlay={menu} trigger={['click']}>
      <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
        Додати шаблон <SelectIcon src={arrow} alt="arrow" />
      </a>
    </DropdownHead>
  );
};

export default TemplateMenu;
