import React from 'react';
import edit from 'img/edit_icon.svg';
import remove from 'img/delete_icon.svg';
import { UserWrapper, Text } from 'views/styled/UsersStyle';
import { ActionIcon, rightColumnStyle } from 'views/styled/PositionStyle';
import { Table } from 'views/styled/ListStyle';
import { Tooltip } from 'antd';

interface IItemType {
  position: string;
  id: number;
  deleteModal: (id: number) => void;
  editModal: (id: number) => void;
  format: string;
}

const TypesItem = ({ position, deleteModal, editModal, id, format }: IItemType) => {
  return (
    <UserWrapper>
      <Table>
        <tbody>
          <tr>
            <td style={{ width: '29%' }}>
              <Text textDark>{position}</Text>
            </td>
            <td style={{ width: '50%' }}>
              <Text textDark>{format}</Text>
            </td>
            <td style={rightColumnStyle}>
              <Tooltip placement="top" title="Редагувати">
                <ActionIcon src={edit} alt="edit" onClick={() => editModal(id)} />
              </Tooltip>

              <Tooltip placement="top" title="Видалити">
                <ActionIcon src={remove} alt="delete" onClick={() => deleteModal(id)} />
              </Tooltip>
            </td>
          </tr>
        </tbody>
      </Table>
    </UserWrapper>
  );
};
export default TypesItem;
