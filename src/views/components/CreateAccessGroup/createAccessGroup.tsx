import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'states/store';
import AccessGroupOptions from './accessGroupOptions';
import { Title } from 'views/styled/DashboardStyle';
import { ButtonPrimary } from 'views/styled/ButtonsStyle';
import { SaveBtnWrapper, Wrapper } from 'views/styled/AccessGroupStyle';
import {
  createAccessGroups,
  getAccessGroupAsync,
  getFoldersAsync,
  updateAccessGroups,
} from 'states/ducks/access-groups/action';
import Indicator from '../Indicator';
import { positionsAsync } from 'states/ducks/positions/action';
import styled from 'styled-components';
import { usersAsync } from '../../../states/ducks/users/action';
import { getComputedPositions } from '../../../states/ducks/positions/selectors';
import { getUsersData } from '../../../states/ducks/users/selectors';

export interface IModalConfig {
  value?: string;
  id: string;
  setVisible: (state: boolean) => void;
  positionsData: any[];
}

export const BtnPadding = styled.span`
  padding: 2px 20px;
`;

const CreateAccessGroup: React.FC<{ match: { params: { id: string | undefined } } }> = ({
  match: {
    params: { id },
  },
}) => {
  const dispatch = useDispatch();
  const getPositions = useCallback(() => dispatch(positionsAsync()), [dispatch]);
  const getFolders = useCallback(() => dispatch(getFoldersAsync()), [dispatch]);
  const getUsers = useCallback(() => dispatch(usersAsync()), [dispatch]);
  const accessGroup = useCallback((id) => dispatch(getAccessGroupAsync(id)), [dispatch]);

  const updateAccessGroup = useCallback(
    (accessGroup) => dispatch(createAccessGroups(accessGroup)),
    [dispatch],
  );
  const createAccessGroup = useCallback(
    (accessGroup) => dispatch(updateAccessGroups(accessGroup)),
    [dispatch],
  );

  useEffect(() => {
    if (id) {
      accessGroup(id);
    }
  }, [accessGroup]);

  useEffect(() => {
    getFolders();
  }, [getFolders]);

  useEffect(() => {
    getUsers();
  }, [getUsers]);

  useEffect(() => {
    getPositions();
  }, [getPositions]);

  const { positions } = useSelector((state: RootState) => state.positions);
  const { selectedAccessGroup, folders } = useSelector((state: RootState) => state.accessGroups);
  const { users } = useSelector((state: RootState) => state.users);

  const { loading: pennding, user, error } = useSelector(getUsersData);

  const initialValues = {
    name: id ? selectedAccessGroup.name : '',
    description: id ? selectedAccessGroup.description : '',
    members: id ? selectedAccessGroup.members : [],
    positions: id ? selectedAccessGroup.positions : [],
    documents: id ? selectedAccessGroup.documents : [],
    folders: id ? selectedAccessGroup.folders : [],
    access: id ? selectedAccessGroup.access : '',
    id: id ? id : '',
  };

  const setValue = (e: any) => {};

  const handleSubmit = () => {
    // if(id){
    //   updateAccessGroup(initialValues)
    // } else createAccessGroup(initialValues)
    //
  };
  return (
    <>
      {selectedAccessGroup ? (
        <Wrapper>
          <Title>{`Група доступу ${initialValues.name}`}</Title>

          <AccessGroupOptions
            setValue={setValue}
            folders={folders}
            accessGroup={initialValues}
            positions={positions}
            users={user}
            id={id}
          />

          <SaveBtnWrapper>
            <ButtonPrimary onClick={handleSubmit} small>
              <BtnPadding>Зберегти</BtnPadding>
            </ButtonPrimary>
          </SaveBtnWrapper>
        </Wrapper>
      ) : (
        <Indicator position="center" />
      )}
    </>
  );
};

export default CreateAccessGroup;
