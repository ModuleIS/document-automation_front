import { Select } from 'antd';
import React, { useState } from 'react';
import { FieldSelect } from '../../../styled/FieldsStyle';

interface ISimpleSelectOptions {
  options: any[];
  placeholder: string;
  defaultValue: string;
}

const SimpleSelect = ({ options, placeholder, defaultValue }: ISimpleSelectOptions) => {
  const [selectValue, setSelectValue] = useState(defaultValue);
  const { Option } = Select;

  const handleChange = (value: any) => {
    setSelectValue(value.id);
  };
  return (
    <>
      <FieldSelect defaultValue={selectValue} placeholder={placeholder} onChange={handleChange}>
        {options.map((item) => (
          <Option value={item.name} key={item.id}>
            {item.name}
          </Option>
        ))}
      </FieldSelect>
    </>
  );
};
export default SimpleSelect;
