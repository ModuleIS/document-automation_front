import React from 'react';
import { useState } from 'react';
import { Select, TreeSelect } from 'antd';
import styled from 'styled-components';
// import arrow from '../../CreateDocumentsType/components/node_modules/img/select_arrow.svg';

interface ICascadeSelect {
  folders: any[];
  placeholder: string;
  defaultValue: any;
}

interface ISubFolder {
  id: number;
  name: string;
  subfolders: any[];
}
export const FieldSelect = styled(TreeSelect)`
  width: 100%;
  max-width: 320px;
  color: #6e6669;

  .ant-select-selection-placeholder {
    color: #6e6669;
    left: 0;
  }

  .ant-select-selector {
    font-size: 18px;
  }

  &.ant-select-single:not(.ant-select-customize-input) .ant-select-selector {
    height: 38px;
  }

  &.ant-select:not(.ant-select-customize-input) .ant-select-selector {
    border: 0;
    border-bottom: 2px solid #000;
    padding: 4px 12px 4px 0;
  }

  &.ant-select-focused:not(.ant-select-disabled).ant-select:not(.ant-select-customize-input)
    .ant-select-selector {
    box-shadow: none;
  }

  .ant-select-arrow-loading {
    font-size: 22px;
    width: 20px;
    height: 20px;
    margin-top: -12px;
  }
`;

const SelectIcon = styled.img`
  cursor: pointer;
  width: 16px;
  height: 7px;
  object-fit: cover;
  margin: 3px 0 0 13px;
`;

const CascadeSelect = ({ folders, placeholder, defaultValue }: ICascadeSelect) => {
  const { TreeNode } = TreeSelect;

  const [selectValue, setSelectValue] = useState(defaultValue);

  const onChange = (value: any) => {
    setSelectValue(value);
  };

  const addTreeFolder = (subfolders: any[]) => {
    return subfolders.map((item: ISubFolder) => (
      <TreeNode value={item.id} title={item.name}>
        {item.subfolders ? addTreeFolder(item.subfolders) : <div />}
      </TreeNode>
    ));
  };

  // const icon = <SelectIcon src={arrow} alt="arrow" />;

  return (
    <FieldSelect
      value={selectValue}
      dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
      placeholder={placeholder}
      showArrow={true}
      multiple
      showSearch={false}
      treeDefaultExpandAll
      onChange={onChange}
    >
      {folders.map((item) => (
        <TreeNode value={item.id} title={item.name}>
          {item.subfolders ? addTreeFolder(item.subfolders) : <div></div>}
        </TreeNode>
      ))}
    </FieldSelect>
  );
};
export default CascadeSelect;
