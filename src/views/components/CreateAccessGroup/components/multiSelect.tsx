import React, { useState } from 'react';
import { Select } from 'antd';
import { FieldSelect } from '../../../styled/FieldsStyle';

interface IOptions {
  name: 'string';
  id: number;
}

interface IMultiSelectOptions {
  options: any[];
  placeholder: string;
  defaultValue: any[];
}

const MultiSelect = ({ options, placeholder, defaultValue }: IMultiSelectOptions) => {
  const [selectValue, setSelectValue] = useState(defaultValue);

  const { Option } = Select;

  const handleChange = (value: any) => {
    setSelectValue(value);
  };

  return (
    <>
      <FieldSelect
        mode="tags"
        value={selectValue}
        placeholder={placeholder}
        showArrow={true}
        showSearch={false}
        onChange={handleChange}
      >
        {options.map((item) => (
          <Option value={item.id} key={item.id}>
            {item.name}
          </Option>
        ))}
      </FieldSelect>
    </>
  );
};

export default MultiSelect;
