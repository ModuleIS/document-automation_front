import React from 'react';
import { FieldGroup } from 'views/styled/FieldsStyle';
import { DescriptionsContainer, FieldWrapper, TitlePrimary } from 'views/styled/AccessGroupStyle';
import styled from 'styled-components';
import MultiSelect from './components/multiSelect';
import SimpleSelect from './components/simpleSelect';
import CascadeSelect from './components/cascadeSelect';

interface IAccessGroupOptions {
  id: string | undefined;
  accessGroup: any;
  positions: any[];
  users: IUserInitial[];
  folders: any;
  setValue: (e: any) => void;
}
interface IUserInitial {
  id: number;
  name: string;
}

export const InputAccess = styled.input`
  font-size: 18px;
  border: 0;
  border-bottom: 2px solid #000;
  padding: 4px 12px 4px 0;
  width: 100%;
`;

export const FieldContainer = styled.div`
  margin: 15px 63px 15px 0;
  width: 320px;
`;

const access = [
  { id: 1, name: 'Перегляд' },
  { id: 2, name: 'Редагування' },
];

const AccessGroupOptions = ({
  id,
  accessGroup,
  positions,
  folders,
  setValue,
  users,
}: IAccessGroupOptions) => {
  return (
    <>
      <FieldGroup>
        <FieldContainer>
          <InputAccess
            name="name"
            onChange={setValue}
            value={accessGroup.name}
            placeholder="Назва групи"
          />
        </FieldContainer>
      </FieldGroup>

      <DescriptionsContainer
        name="description"
        placeholder="Опис"
        onChange={setValue}
        value={accessGroup.description}
      />

      <TitlePrimary>Учасники групи</TitlePrimary>

      <FieldGroup>
        <FieldContainer>
          <MultiSelect
            options={users}
            placeholder="Користувачі"
            defaultValue={accessGroup.members}
          />
        </FieldContainer>
        <FieldContainer>
          <MultiSelect
            options={positions}
            placeholder="Посади"
            defaultValue={accessGroup.positions}
          />
        </FieldContainer>
      </FieldGroup>

      <TitlePrimary>Доступ до</TitlePrimary>

      <FieldGroup>
        <FieldContainer>
          <MultiSelect
            options={positions}
            placeholder="Документи"
            defaultValue={accessGroup.documents}
          />
        </FieldContainer>

        <FieldContainer>
          <CascadeSelect folders={folders} placeholder="Теки" defaultValue={accessGroup.folders} />
        </FieldContainer>
      </FieldGroup>

      <FieldContainer>
        <SimpleSelect options={access} placeholder="Право на" defaultValue={'Перегляд'} />
      </FieldContainer>
    </>
  );
};

export default AccessGroupOptions;
