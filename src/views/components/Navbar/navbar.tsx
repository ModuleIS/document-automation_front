import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import { setUserLogout } from 'states/ducks/auth/action';
import { RootState } from 'states/store';

const Container = styled.div`
  display: flex;
  justify-content: flex-end;
  width: 100%;
  max-width: 1440px;
  margin: auto;
  padding: 25px 35px;
`;

const Action = styled.div`
  position: relative;
  top: 4px;
  margin-left: 22px;
`;

const UserName = styled.div`
  font-size: 22px;
  color: #000;
`;

const Navbar = () => {
  const dispatch = useDispatch();
  const userLogout = useCallback(() => dispatch(setUserLogout()), [dispatch]);

  const user = useSelector((state: RootState) => state.login.user);

  return (
    <Container>
      <UserName>{user}</UserName>
      <Action>
        <span aria-label="Вийти" role="button" onClick={userLogout}>
          <svg viewBox="0 0 512 512" height="24" width="24" xmlns="http://www.w3.org/2000/svg">
            <path d="M480.971 239.029l-90.51-90.509a24 24 0 00-33.942 0 24 24 0 000 33.941L406.059 232H144a24 24 0 00-24 24 24 24 0 0024 24h262.059l-49.54 49.539a24 24 0 0033.942 33.941l90.51-90.51a24 24 0 000-33.941z" />
            <path d="M304 392a24 24 0 00-24 24v24H72V72h208v24a24 24 0 0048 0V64a40 40 0 00-40-40H64a40 40 0 00-40 40v384a40 40 0 0040 40h224a40 40 0 0040-40v-32a24 24 0 00-24-24z" />
          </svg>
        </span>
      </Action>
    </Container>
  );
};

export default Navbar;
