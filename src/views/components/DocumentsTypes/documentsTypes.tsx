import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Indicator from 'views/components/Indicator';
import { RootState } from 'states/store';
import { ButtonPrimary } from 'views/styled/ButtonsStyle';
import { SearchField } from 'views/styled/SearchStyle';
import { ActionButton, ModalDeleteWrapper, subTitle } from 'views/styled/PositionStyle';
import { Empty, Modal } from 'antd';
import { Title, TopPanel } from 'views/styled/DashboardStyle';
import PositionItem from '../UsersPositions/components/position-item';
import { BtnPadding } from '../CreateAccessGroup/createAccessGroup';
import { config } from 'utilities/helpers/configDialog';
import { useHistory } from 'react-router-dom';
import { deleteDocumentType, getDocumentsTypes } from 'states/ducks/documents-types/action';
import { IDocumentsType } from 'states/ducks/documents-types/index.d';
import { CategoryText } from 'views/styled/CategoryStyle';
import { List } from 'views/styled/ListStyle';

import { Wrapper, Filters } from 'views/styled/UsersStyle';

const Data = [
  { name: 'Акт', id: 22 },
  { name: 'Заява', id: 222 },
];

const DocumentsTypes = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const getDocumentTypes = useCallback(() => dispatch(getDocumentsTypes()), [dispatch]);
  const deleteDocumentsType = useCallback((id) => dispatch(deleteDocumentType(id)), [dispatch]);

  useEffect(() => {
    getDocumentTypes();
  }, [getDocumentTypes]);

  const { loading, documentsTypes, error } = useSelector(
    (state: RootState) => state.documentsTypes,
  );

  const openEditDocumentType = (id: number) => {
    history.push(`/document_type/create_type/${id}`);
  };

  const createDocumentType = () => {
    history.push(`/document_type/create_type`);
  };

  const handleClickDeleteDocumentsTypes = (modalClose: () => void, id: number): void => {
    if (id) {
      deleteDocumentsType(id);
      modalClose();
    }
  };

  const openDeleteDocumentType = (id: number): void => {
    const modal = Modal.info({
      ...config,
      title: 'Ви впевнені, що хочете видалити тип документа?',
      bodyStyle: { textAlign: 'center' },
      content: (
        <>
          <ModalDeleteWrapper>
            <ActionButton backgroundColor={'#F1ACAC'} onClick={() => modal.destroy()}>
              Скасувати
            </ActionButton>

            <ActionButton
              onClick={() => handleClickDeleteDocumentsTypes(modal.destroy, id)}
              backgroundColor={'#C2FFBF'}
            >
              Видалити
            </ActionButton>
          </ModalDeleteWrapper>
        </>
      ),
    });
  };

  return (
    <>
      <TopPanel>
        <Title>Типи документів</Title>
        <Wrapper>
          <SearchField placeholder="Пошук" allowClear />

          <ButtonPrimary small onClick={createDocumentType}>
            <BtnPadding> Додати тип </BtnPadding>
          </ButtonPrimary>
        </Wrapper>
        <Filters>
          <CategoryText>
            {' '}
            <span style={subTitle}>Тип документу</span>{' '}
          </CategoryText>
        </Filters>
      </TopPanel>
      <List
        height={292}
      >
        { !loading && !documentsTypes.length && <Empty description="Нічого не знайдено" image={Empty.PRESENTED_IMAGE_SIMPLE} /> }
        {!!loading ? (
          <Indicator position="center" />
        ) : (
          documentsTypes.map(({ name, id }: IDocumentsType) => (
            <PositionItem
              key={id}
              id={id}
              position={name}
              deleteModal={openDeleteDocumentType}
              editModal={openEditDocumentType}
            />
          ))
        )}
      </List>
    </>
  );
};

export default DocumentsTypes;
