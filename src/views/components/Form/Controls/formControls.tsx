import React from 'react';

import { ForgotPassword, FormButton, FormButtonWrapper } from 'views/styled/FormStyle';
import { Buttons } from 'views/styled/ButtonsStyle';

interface IFormControls {
  isValid: boolean,
  isSubmitting: boolean,
  onClearErrors: () => void
}

const FormControls = ({ isValid, isSubmitting, onClearErrors }: IFormControls) => {
  return (
    <Buttons 
      between={false} 
      direction="row"
    >
      <ForgotPassword to="/auth/reset_password" onClick={ onClearErrors }>Відновити пароль</ForgotPassword>
      <FormButtonWrapper>
        <FormButton 
          type="primary" 
          htmlType="submit" 
          disabled={!isValid || !!isSubmitting}
        >
          Увійти
        </FormButton>
      </FormButtonWrapper>
    </Buttons>
  );
};

export default FormControls;
