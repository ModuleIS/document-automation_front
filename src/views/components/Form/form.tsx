import React, { useEffect } from 'react';
import { Formik, Form, useFormikContext } from 'formik';

interface IFormContainer {
  children: React.ReactNode,
  values: Object,
  validationSchema: Object,
  successfully?: boolean,
  notSuccessful?: boolean,
  onSubmit: (data: any) => void
}

type ShapeResetType = {
  notSuccessful: boolean | undefined,
  successfully: boolean | undefined
};

const ShapeReset = ({notSuccessful, successfully}: ShapeResetType) => {
  const { setSubmitting, resetForm } = useFormikContext();

  useEffect(() => {
    if (successfully || notSuccessful) {
      setSubmitting(false);
    }

    if (successfully) {
      resetForm();
    }
  }, [successfully, notSuccessful, setSubmitting, resetForm]);

  return null;
}

const FormBase = ({ children, values, validationSchema, successfully, notSuccessful, onSubmit }: IFormContainer) => {
  const handleClickSubmit = (values: Object): void => {
    onSubmit(values);
  };

  return (
    <Formik 
      initialValues={values} 
      validationSchema={validationSchema} 
      onSubmit={handleClickSubmit}
    >
      {(props) => (
        <>
          <Form>
            {React.Children.map(children, (child: any) =>
              React.cloneElement(child, {
                ...props,
              }),
            )}
          </Form>
          <ShapeReset
            successfully={ successfully }
            notSuccessful={ notSuccessful }
          />
        </>
      )}
    </Formik>
  );
};

export default FormBase;
