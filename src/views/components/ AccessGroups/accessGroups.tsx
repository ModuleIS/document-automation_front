import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Wrapper, Filters } from 'views/styled/UsersStyle';
import Indicator from 'views/components/Indicator';
import { RootState } from 'states/store';
import PositionItem from '../UsersPositions/components/position-item';
import { ButtonPrimary } from 'views/styled/ButtonsStyle';
import { SearchField } from 'views/styled/SearchStyle';
import { Title, TopPanel } from 'views/styled/DashboardStyle';
import { useHistory } from 'react-router-dom';
import { ActionButton, ModalDeleteWrapper } from 'views/styled/PositionStyle';
import { deleteAccessGroup, getAccessGroups } from 'states/ducks/access-groups/action';
import { CategoryText } from 'views/styled/CategoryStyle';
import { List } from 'views/styled/ListStyle';
import { Empty, Modal } from 'antd';
import { config } from 'utilities/helpers/configDialog';

interface IAccessGroupContent {
  id: number;
  name: string;
}

const AccessGroups = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const accessGroupsData = useCallback(() => dispatch(getAccessGroups()), [dispatch]);
  const deleteGroup = useCallback((id) => dispatch(deleteAccessGroup(id)), [dispatch]);

  useEffect(() => {
    accessGroupsData();
  }, [accessGroupsData]);

  const { loading, accessGroups } = useSelector((state: RootState) => state.accessGroups);

  const openEditAccessGroup = (id: number) => {
    history.push(`/access_groups/create_access_group/${id}`);
  };

  const createAccessGroup = () => {
    history.push(`/access_groups/create_access_group`);
  };

  const handleClickDeleteAccessGroup = (modalClose: () => void, id: number): void => {
    if (id) {
      deleteGroup(id);
      modalClose();
    }
  };

  const openDeleteAccessGroup = (id: number): void => {
    const modal = Modal.info({
      ...config,
      title: 'Ви впевнені, що хочете видалити групу доступу?',
      bodyStyle: { textAlign: 'center' },
      content: (
        <>
          <ModalDeleteWrapper>
            <ActionButton backgroundColor={'#F1ACAC'} onClick={() => modal.destroy()}>
              Скасувати
            </ActionButton>

            <ActionButton
              onClick={() => handleClickDeleteAccessGroup(modal.destroy, id)}
              backgroundColor={'#C2FFBF'}
            >
              Видалити
            </ActionButton>
          </ModalDeleteWrapper>
        </>
      ),
    });
  };

  return (
    <>
      <TopPanel>
        <Title>Групи доступу</Title>
        <Wrapper>
          <SearchField placeholder="Пошук" allowClear />

          <ButtonPrimary small onClick={createAccessGroup}>
            Додати групу доступу
          </ButtonPrimary>
        </Wrapper>
        <Filters>
          <CategoryText>Групи доступу</CategoryText>
        </Filters>
      </TopPanel>
      <List
        height={292}
      >
        { !loading && !accessGroups.length && <Empty description="Нічого не знайдено" image={Empty.PRESENTED_IMAGE_SIMPLE} /> }
        {!!loading ? (
          <Indicator position="center" />
        ) : (
          accessGroups.map(({ name, id }: IAccessGroupContent) => (
            <PositionItem
              key={id}
              id={id}
              position={name}
              deleteModal={openDeleteAccessGroup}
              editModal={openEditAccessGroup}
            />
          ))
        )}
      </List>
    </>
  );
};

export default AccessGroups;
