import React, { useState, CSSProperties } from 'react';
import { useHistory } from 'react-router-dom';

import FormResetPassword from './components/formResetPassword';
import { resetPassword } from 'states/utils/API/authAPI';
import { Result } from 'antd';
import { ButtonPrimary } from 'views/styled/ButtonsStyle';

const inlineStyle: CSSProperties = {
  fontSize: '16px',
  color: '#000',
  marginTop: '15px',
};

const ResetPassword = () => {
  const [successfully, setSuccessfully] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  const { goBack } = useHistory();

  const sendMail = (postData: { email: string }) => {
    setLoading(true);

    resetPassword(postData).then(
      (data) => {
        setSuccessfully(data);
        setLoading(false);
      },
      (err) => {
        console.log(err);
      },
    );
  };

  return !successfully ? (
    <FormResetPassword onSubmit={sendMail} loading={loading} />
  ) : (
    <Result
      status="success"
      title="Перевірити свою електронну пошту"
      subTitle={
        <p style={inlineStyle}>
          Ми надіслали вам електронний лист із посиланням для
          <br /> скидання пароля. Будь ласка, перевірте це.
        </p>
      }
      extra={
        <ButtonPrimary small={false} onClick={goBack}>
          Повернутися назад
        </ButtonPrimary>
      }
    />
  );
};

export default ResetPassword;
