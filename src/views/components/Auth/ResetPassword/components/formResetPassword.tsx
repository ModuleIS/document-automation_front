import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';
import { Button } from 'antd';

import Form from 'views/components/Form';

import InputField from 'views/UI/Inputs/inputField';
import { Wrapper, FormButton } from 'views/styled/FormStyle';
import { Buttons } from 'views/styled/ButtonsStyle';

import { recoveryPasswordSchema } from 'utilities/helpers/validationSchem';

interface IResetPassword {
  onSubmit: (data: { email: string }) => void;
  loading: boolean;
}

const Title = styled.div`
  font-size: 28px;
  font-weight: 500;
  line-height: 1.32;
`;

const Subtitle = styled.div`
  font-size: 14px;
  color: #9a9a9a;
  margin-top: 12px;
`;

const ButtonBack = styled(Button)`
  font-size: 18px;
  font-weight: 500;
  color: #949494;
  padding: 0;

  &:hover,
  &:focus {
    color: #cecece;
  }

  &:active {
    color: #cecece;
  }

  @media (max-width: 560px) {
    position: absolute;
    top: 2px;
    left: 35px;
  }
`;

const ResetPassword = ({ onSubmit, loading }: IResetPassword) => {
  const { goBack } = useHistory();

  return (
    <Form values={{ email: '' }} validationSchema={recoveryPasswordSchema} onSubmit={onSubmit}>
      <Wrapper>
        <Title>Відновлення пароля</Title>
        <Subtitle>На цей email ми надішлемо посилання для відновлення пароля</Subtitle>
        <>
          <InputField name="email" type="text" placeholder="Наприклад example@mail.com" />
          <Buttons between direction="row">
            <ButtonBack type="link" onClick={goBack}>
              Назад
            </ButtonBack>
            <FormButton type="primary" htmlType="submit" disabled={!!loading}>
              {!!loading ? 'Відправляю...' : 'Продовжити'}
            </FormButton>
          </Buttons>
        </>
      </Wrapper>
    </Form>
  );
};

export default ResetPassword;
