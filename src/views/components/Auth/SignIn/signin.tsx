import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Alert } from 'antd';

import { Wrapper } from 'views/styled/FormStyle';
import InputField from 'views/UI/Inputs/inputField';
import Form from 'views/components/Form';
import FormControls from 'views/components/Form/Controls';
import Intro from 'views/components/Intro';
import { RootState } from 'states/store';
import { signInSchema } from 'utilities/helpers/validationSchem';
import { requestUserLogin, clearUserErrors } from 'states/ducks/auth/action';

const eyeTwoTone = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 469.333 469.333"
    width="20"
    height="20"
    style={{ opacity: '0.6' }}
  >
    <path d="M234.667 170.667c-35.307 0-64 28.693-64 64s28.693 64 64 64 64-28.693 64-64-28.694-64-64-64z" />
    <path d="M234.667 74.667C128 74.667 36.907 141.013 0 234.667c36.907 93.653 128 160 234.667 160 106.773 0 197.76-66.347 234.667-160-36.907-93.654-127.894-160-234.667-160zm0 266.666c-58.88 0-106.667-47.787-106.667-106.667S175.787 128 234.667 128s106.667 47.787 106.667 106.667-47.787 106.666-106.667 106.666z" />
  </svg>
);

const eyeInvisibleOutlined = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 469.44 469.44"
    width="20"
    height="20"
    style={{ opacity: '0.6' }}
  >
    <path d="M231.147 160.373l67.2 67.2.32-3.52c0-35.307-28.693-64-64-64l-3.52.32z" />
    <path d="M234.667 117.387c58.88 0 106.667 47.787 106.667 106.667 0 13.76-2.773 26.88-7.573 38.933l62.4 62.4c32.213-26.88 57.6-61.653 73.28-101.333-37.013-93.653-128-160-234.773-160-29.867 0-58.453 5.333-85.013 14.933l46.08 45.973c12.052-4.693 25.172-7.573 38.932-7.573zM21.333 59.253l48.64 48.64 9.707 9.707C44.48 145.12 16.64 181.707 0 224.053c36.907 93.653 128 160 234.667 160 33.067 0 64.64-6.4 93.547-18.027l9.067 9.067 62.187 62.293 27.2-27.093L48.533 32.053l-27.2 27.2zM139.307 177.12l32.96 32.96c-.96 4.587-1.6 9.173-1.6 13.973 0 35.307 28.693 64 64 64 4.8 0 9.387-.64 13.867-1.6l32.96 32.96c-14.187 7.04-29.973 11.307-46.827 11.307-58.88 0-106.667-47.787-106.667-106.667 0-16.853 4.267-32.64 11.307-46.933z" />
  </svg>
);

const FormContent = ({ isValid, isSubmitting, onClearErrors }: any) => (
  <Wrapper>
    <InputField 
      name="firstname" 
      type="text" 
      placeholder="Ваше ім’я" 
    />
    <InputField 
      name="patronymic" 
      type="text" 
      placeholder="По батькові" 
    />
    <InputField 
      name="secondname" 
      type="text" 
      placeholder="Прізвище" 
    />
    <InputField
      name="password"
      type="password"
      placeholder="Пароль"
      iconRender={(visible: boolean) => (visible ? eyeTwoTone : eyeInvisibleOutlined)}
    />
    <FormControls 
      isValid={isValid} 
      isSubmitting={isSubmitting}
      onClearErrors={ onClearErrors }
    />
  </Wrapper>
);

const SignUp = () => {
  const dispatch = useDispatch();
  const requestLogin = useCallback((user) => dispatch(requestUserLogin(user)), [dispatch]);
  const clearErrors = useCallback(() => dispatch(clearUserErrors()), [dispatch]);

  const {error, isAuthenticated} = useSelector((state: RootState) => state.login);
  const initialValues = {
    firstname: '',
    patronymic: '',
    secondname: '',
    password: '',
  };

  return (
    <>
      <Intro />
      <Form 
        values={initialValues} 
        validationSchema={signInSchema} 
        onSubmit={requestLogin}
        successfully={ isAuthenticated }
        notSuccessful={ !!error }
      >
        <FormContent
          onClearErrors={ clearErrors }
        />
      </Form>
      {error && (
        <Alert
          message={ error }
          type="error"
          showIcon
          closable
          style={{maxWidth: '320px', margin: 'auto'}}
        />
      )}
    </>
  );
};

export default SignUp;
