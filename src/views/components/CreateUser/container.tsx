import React, { useState, useEffect, useCallback, useRef, MutableRefObject } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';
import { Modal, message } from 'antd';

import Form from 'views/components/Form';
import FormContent from './presenter';

import { createUserSchema } from 'utilities/helpers/validationSchem';
import { config } from 'utilities/helpers/configDialog';
import { createUserAsync, setDiscard } from 'states/ducks/user/action';
import { createPositionAsync } from 'states/ducks/positions/action';
import { getComputedPositions } from 'states/ducks/positions/selectors';
import { getPasswod } from 'states/utils/API/createUserAPI';
import { RootState } from 'states/store';

import { Title, Head } from 'views/styled/DashboardStyle';
import { ButtonPrimary, Buttons } from 'views/styled/ButtonsStyle';
import { FormInput } from 'views/styled/FieldsStyle';
import ButtonBack from 'views/UI/buttons/buttonBack';

const Wrapper = styled.div`
  padding: 50px 25px 12px 25px;
  background-color: #fff;
  height: 100%;
`;

const CreateUser = () => {
  const dispatch = useDispatch();

  const createUser = useCallback((user) => dispatch(createUserAsync(user)), [dispatch]);
  const userDiscard = useCallback(() => dispatch(setDiscard()), [dispatch]);
  const createPosition = useCallback((position) => dispatch(createPositionAsync(position)), [
    dispatch,
  ]);

  const [comeBack, setComeBack] = useState<boolean>(false);

  const { loadingUser, successfully, errorUser } = useSelector(
    (state: RootState) => state.createUser,
  );
  const { loadingPositions, positions, errorPositions } = useSelector(getComputedPositions);

  const { goBack } = useHistory();
  const { pathname } = useLocation();
  const inputRef: MutableRefObject<any> = useRef(null);

  const initialValues = {
    firstname: '',
    secondname: '',
    patronymic: '',
    position: '',
    role: '',
    phone: '',
    email: '',
    password: '',
  };
  const mutablePaths: string = pathname.split('/').filter((i) => i)[1];
  const switchText: string = mutablePaths === 'create' ? 'Додати користувача' : 'Сберегти';
  const errors: string | null = errorUser ?? errorPositions;

  useEffect(() => {
    if (successfully) {
      successfullyCreatedUser();
    }

    if (errors && !comeBack) {
      message.error(errors);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [successfully, errors, comeBack]);

  const handleClickSavePosition = (modalClose: () => void): void => {
    const { value } = inputRef.current.input;

    if (value) {
      createPosition({ name: value });
    }

    if (errorUser) {
      userDiscard();
    }

    modalClose();
  };

  const handleClick = async (setValue: (field: string, value: any) => void) => {
    try {
      const password: string = await getPasswod();
      setValue('password', password);
    } catch (err) {
      console.log(err);
    }
  };

  const handleClickBack = (): void => {
    goBack();

    if (successfully || errors) {
      userDiscard();
      setComeBack(true);
    }
  };

  const handleSetPosition = (): void => {
    const modal = Modal.info({
      ...config,
      title: 'Додати посаду',
      bodyStyle: { textAlign: 'right' },
      content: (
        <>
          <FormInput placeholder="Назва посади" ref={inputRef} autoFocus />
          <ButtonPrimary small onClick={() => handleClickSavePosition(modal.destroy)}>
            Додати посаду
          </ButtonPrimary>
        </>
      ),
    });
  };

  const successfullyCreatedUser = () => {
    const modal = Modal.info({
      ...config,
      maskClosable: false,
      bodyStyle: { textAlign: 'center' },
      content: (
        <>
          <svg viewBox="0 0 116 121" width="65" height="65">
            <g fill="none" fillRule="evenodd">
              <path
                stroke="#CACDD6"
                strokeLinecap="round"
                d="M41.529 91.929H.5V.5h74.285v45.828M17.643 23.357h40M17.643 40.5h40M50.843 57.643h-33.2"
              ></path>
              <path
                d="M40.5 83.357c0-20.513 16.63-37.143 37.143-37.143s37.142 16.63 37.142 37.143c0 20.514-16.629 37.144-37.142 37.144-20.514 0-37.143-16.63-37.143-37.144z"
                stroke="#212A46"
                strokeLinecap="round"
              ></path>
              <path
                d="M92.27 68.84c-1.012-.91-2.272.111-5.274 2.695-2.08 1.786-3.515 3.198-5.44 5.176-5.07 5.216-8.76 10.137-10.084 11.974l-5.651-3.26c-1.406-.884-2.141-1.19-2.761-.673a1.355 1.355 0 0 0-.413.812c-.045.612.327 1.147 1.12 2.044 3.6 4.085 8.898 10.243 8.9 10.245a1 1 0 0 0 1.68-.263c2.488-5.847 6.068-11.823 10.64-17.764.567-.738 1.083-1.425 1.57-2.078 1.114-1.486 2.075-2.77 3.205-4.027 2.338-2.6 2.922-3.355 2.879-4.103a1.264 1.264 0 0 0-.37-.779"
                fill="#00D093"
              ></path>
            </g>
          </svg>
          <Title style={{ marginTop: '20px' }}>Ви успішно створили користувача</Title>
          <Buttons between direction="column">
            <ButtonPrimary
              small={false}
              onClick={() => {
                modal.destroy();
                userDiscard();
              }}
            >
              Створити нового користувача
            </ButtonPrimary>
          </Buttons>
        </>
      ),
    });
  };

  return (
    <Wrapper>
      <Head bottom={60} isShow={mutablePaths === 'create'}>
        <ButtonBack onClick={handleClickBack} />
        <Title>Додати нового користувача</Title>
      </Head>
      <Form 
        values={initialValues} 
        validationSchema={createUserSchema}
        successfully={ successfully }
        onSubmit={createUser}
      >
        <FormContent
          onSetPosition={handleSetPosition}
          onClick={handleClick}
          positions={positions}
          loadingPosition={loadingPositions}
          loading={loadingUser}
          switchText={switchText}
        />
      </Form>
    </Wrapper>
  );
};

export default CreateUser;
