import React from 'react';
import styled from 'styled-components';
import { Tooltip } from 'antd';

import InputField from 'views/UI/Inputs/inputField';
import SelectField from 'views/UI/selects/selectField';
import { FieldGroup } from 'views/styled/FieldsStyle';

const FieldWrapper = styled.div`
  position: relative;

  span[role='button'] {
    position: absolute;
    top: 15px;
    right: 65px;
    cursor: pointer;
  }
`;

interface IPersonalUserDetails {
  options: any[];
  loading: boolean;
  onClickSetOption: () => void;
}

const PersonalUserDetails = ({ loading, options, onClickSetOption }: IPersonalUserDetails) => {
  return (
    <>
      <FieldGroup>
        <InputField name="secondname" type="text" placeholder="Прізвище" />
        <InputField name="firstname" type="text" placeholder="Ім’я" />
      </FieldGroup>
      <FieldGroup>
        <InputField name="patronymic" type="text" placeholder="По батькові" />
      </FieldGroup>
      <FieldGroup>
        <FieldWrapper>
          <SelectField placeholder="Посада" name="position" loading={loading} options={options} />
          <Tooltip placement="top" title="Додати посаду">
            <span aria-label="додати посаду" role="button" onClick={onClickSetOption}>
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 492 492">
                <path d="M465.064 207.566h.028-180.656V27.25c0-14.84-12.016-27.248-26.856-27.248h-23.116c-14.836 0-26.904 12.408-26.904 27.248v180.316H26.908c-14.832 0-26.908 12-26.908 26.844v23.248c0 14.832 12.072 26.78 26.908 26.78h180.656v180.968c0 14.832 12.064 26.592 26.904 26.592h23.116c14.84 0 26.856-11.764 26.856-26.592V284.438h180.624c14.84 0 26.936-11.952 26.936-26.78V234.41c0-14.844-12.096-26.844-26.936-26.844z" />
              </svg>
            </span>
          </Tooltip>
        </FieldWrapper>
        <SelectField
          placeholder="Роль"
          name="role"
          options={[
            { label: 'Адміністратор', value: 'ADMIN', id: 1 },
            { label: 'Користувач', value: 'USER', id: 2 },
          ]}
        />
      </FieldGroup>
      <FieldGroup>
        <SelectField
          placeholder="Доступ до документів"
          name="documents"
          mode="multiple"
          showArrow
          showSearch={false}
          options={[]}
        />
      </FieldGroup>
    </>
  );
};

export default PersonalUserDetails;
