import React from 'react';
import styled from 'styled-components';
import { Tooltip } from 'antd';

import InputField from 'views/UI/Inputs/inputField';
import { ButtonPrimary } from 'views/styled/ButtonsStyle';

interface IContactUserDetails {
  onClick: (cb: (field: string, value: any) => void) => void;
}

const FieldGroup = styled.div`
  display: flex;

  .ant-form-item {
    min-width: 320px;
    margin-right: 95px;
  }
`;

const Title = styled.div`
  font-size: 22px;
  color: #000;
  margin-bottom: 40px;
`;

const eyeTwoTone = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 469.333 469.333"
    width="20"
    height="20"
    style={{ opacity: '0.6' }}
  >
    <path d="M234.667 170.667c-35.307 0-64 28.693-64 64s28.693 64 64 64 64-28.693 64-64-28.694-64-64-64z" />
    <path d="M234.667 74.667C128 74.667 36.907 141.013 0 234.667c36.907 93.653 128 160 234.667 160 106.773 0 197.76-66.347 234.667-160-36.907-93.654-127.894-160-234.667-160zm0 266.666c-58.88 0-106.667-47.787-106.667-106.667S175.787 128 234.667 128s106.667 47.787 106.667 106.667-47.787 106.666-106.667 106.666z" />
  </svg>
);

const eyeInvisibleOutlined = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 469.44 469.44"
    width="20"
    height="20"
    style={{ opacity: '0.6' }}
  >
    <path d="M231.147 160.373l67.2 67.2.32-3.52c0-35.307-28.693-64-64-64l-3.52.32z" />
    <path d="M234.667 117.387c58.88 0 106.667 47.787 106.667 106.667 0 13.76-2.773 26.88-7.573 38.933l62.4 62.4c32.213-26.88 57.6-61.653 73.28-101.333-37.013-93.653-128-160-234.773-160-29.867 0-58.453 5.333-85.013 14.933l46.08 45.973c12.052-4.693 25.172-7.573 38.932-7.573zM21.333 59.253l48.64 48.64 9.707 9.707C44.48 145.12 16.64 181.707 0 224.053c36.907 93.653 128 160 234.667 160 33.067 0 64.64-6.4 93.547-18.027l9.067 9.067 62.187 62.293 27.2-27.093L48.533 32.053l-27.2 27.2zM139.307 177.12l32.96 32.96c-.96 4.587-1.6 9.173-1.6 13.973 0 35.307 28.693 64 64 64 4.8 0 9.387-.64 13.867-1.6l32.96 32.96c-14.187 7.04-29.973 11.307-46.827 11.307-58.88 0-106.667-47.787-106.667-106.667 0-16.853 4.267-32.64 11.307-46.933z" />
  </svg>
);

const ContactUserDetails = ({ onClick }: IContactUserDetails) => (
  <>
    <Title>Контактні дані</Title>
    <FieldGroup>
      <InputField name="phone" type="tel" placeholder="Телефон" />
      <InputField name="email" type="text" placeholder="Пошта" />
    </FieldGroup>
    <FieldGroup>
      <InputField
        name="password"
        type="password"
        placeholder="Пароль"
        iconRender={(visible: boolean) => (visible ? eyeTwoTone : eyeInvisibleOutlined)}
        setValue={(setValue: (field: string, value: any) => void) => (
          <Tooltip placement="top" title="Згенерувати пароль">
            <span role="button" aria-label="згенерувати пароль" onClick={() => onClick(setValue)}>
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="16" height="16">
                <path d="M463.748 48.251c-64.336-64.336-169.013-64.335-233.349.001-43.945 43.945-59.209 108.706-40.181 167.461L4.396 401.536a14.988 14.988 0 00-4.395 10.606V497c0 8.291 6.709 15 15 15h84.858c3.984 0 7.793-1.582 10.605-4.395l21.211-21.226a15.002 15.002 0 004.292-12.334l-2.637-22.793 31.582-2.974a14.975 14.975 0 0013.521-13.521l2.974-31.582 22.793 2.651c4.233.571 8.496-.85 11.704-3.691a15.04 15.04 0 005.024-11.206V363h27.422c3.984 0 7.793-1.582 10.605-4.395l38.467-37.958c58.74 19.043 122.381 4.929 166.326-39.046 64.336-64.335 64.336-169.014 0-233.35zm-42.435 106.07c-17.549 17.549-46.084 17.549-63.633 0s-17.549-46.084 0-63.633 46.084-17.549 63.633 0 17.548 46.084 0 63.633z" />
              </svg>
            </span>
          </Tooltip>
        )}
      />
    </FieldGroup>
  </>
);

export default ContactUserDetails;
