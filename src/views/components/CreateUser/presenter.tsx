import React from 'react';

import ContactUserDetails from './components/contactUserDetails';
import PersonalUserDetails from './components/personalUserDetails';

import { FormButton } from 'views/styled/FormStyle';

interface IPresenter {
  onSetPosition: () => void,
  onClick: (setValue: (field: string, value: any) => void) => Promise<any>,
  positions: Array<{ id: number; value: string; label: string }>,
  loadingPosition: boolean,
  loading: boolean,
  switchText: string,
  isValid?: boolean,
  isSubmitting?: boolean
}

const Presenter = ({
  onSetPosition,
  onClick,
  positions,
  loadingPosition,
  loading,
  switchText,
  isValid,
  isSubmitting
}: IPresenter) => {
  return (
    <>
      <PersonalUserDetails
        onClickSetOption={onSetPosition}
        options={positions}
        loading={loadingPosition}
      />
      <ContactUserDetails 
        onClick={onClick}
      />
      <FormButton
        type="primary"
        htmlType="submit"
        style={{ marginTop: '20px' }}
        disabled={!isValid || !!isSubmitting}
      >
        {!loading ? switchText : 'Cтворюю...'}
      </FormButton>
    </>
  );
};

export default Presenter;
