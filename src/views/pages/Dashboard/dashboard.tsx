import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import { Sidebar, SidebarWrapper, Link } from 'views/styled/SidebarStyle';
import { ManageWrapper, Layout, MainContent } from 'views/styled/DashboardStyle';

import Users from 'views/components/Users';
import CreateUser from 'views/components/CreateUser';
import UsersPositions from 'views/components/UsersPositions';
import CreateAccessGroup from 'views/components/CreateAccessGroup';
import AccessGroups from 'views/components/ AccessGroups';
import User from 'views/components/User';
import Navbar from 'views/components/Navbar';
import DocumentsTypes from 'views/components/DocumentsTypes';
import CreateDocumentsType from 'views/components/CreateDocumentsType';
import Documents from 'views/components/Documents';

const Dashboard = () => (
  <ManageWrapper>
    <Navbar />
    <Layout>
      <Sidebar>
        <SidebarWrapper>
          <ul>
            <li>
              <Link to="/documents">Документи</Link>
            </li>
            <li>
              <Link to="/task">Завдання</Link>
            </li>
            <li>
              <Link to="/messages">Повідомлення</Link>
            </li>
            <li>
              <Link to="/users">Користувачі</Link>
            </li>
            <li>
              <Link to="/users_positions">Посади</Link>
            </li>
            <li>
              <Link to="/access_groups">Групи доступу</Link>
            </li>
            <li>
              <Link to="/document_type">Тип документів</Link>
            </li>
            <li>
              <Link to="/certificate">Довідка</Link>
            </li>
          </ul>
        </SidebarWrapper>
      </Sidebar>
      <MainContent>
        <Switch>
          <Route 
            exact 
            path="/users" 
            component={Users} 
          />
          <Route 
            exact 
            path="/users/create" 
            component={CreateUser} 
          />
          <Route 
            path="/users/:id" 
            component={User} 
          />
          <Route 
            path="/users_positions" 
            component={UsersPositions} 
          />
          <Route 
            exact
            path="/access_groups" 
            component={AccessGroups} 
          />
          <Route 
            exact 
            path="/access_groups/create_access_group" 
            component={CreateAccessGroup} 
          />
          <Route 
            path="/access_groups/create_access_group/:id" 
            component={CreateAccessGroup} 
          />
          <Route 
            exact 
            path="/document_type" 
            component={DocumentsTypes} 
          />
          <Route 
            exact 
            path="/document_type/create_type" 
            component={CreateDocumentsType} 
          />
          <Route 
            exact 
            path="/document_type/create_type/:id" 
            component={CreateDocumentsType} 
          />
          <Route 
            path="/documents" 
            component={Documents} 
          />
          <Redirect from='/' to='/documents'/>
        </Switch>
      </MainContent>
    </Layout>
  </ManageWrapper>
);

export default Dashboard;
