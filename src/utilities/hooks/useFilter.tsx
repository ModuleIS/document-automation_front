import { useState, useEffect } from 'react';
import { SelectValue } from 'antd/lib/select';

import storage from 'utilities/helpers/storage';

type DataType = {
  [key: string]: any
}

const useFilter = (data: DataType, getFilter: (params?: object) => void) => {
  const [values, setValues] = useState<DataType>(data);
  const [isDisabled, setIsDisabled] = useState<boolean>(true);
  const storageValue = storage.get('filter_users');

  useEffect(() => {
    if (storageValue) {
      setIsDisabled(false);
    }
  }, [storageValue, setIsDisabled]);

  const handleFilter = (value: SelectValue, key: string): void => {
    const dynamicValue = key === 'position' ? {name: value} : value;

    setValues((prevState: DataType) => ({...prevState, [key]: value}));
    getFilter({...storageValue, [key]: dynamicValue});
  };

  const handleClearFilter = (): void => {
    getFilter();
    setValues({role: null,position: null,status: null});
    setIsDisabled(true);
  }

  return {
    values,
    isDisabled,
    onFilters: handleFilter,
    onClearFilter: handleClearFilter
  };
};

export { useFilter };
