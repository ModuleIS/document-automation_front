import Cookies from 'js-cookie';

class SessionCookie {
  getHasAccess() {
    return !!Cookies.get('access');
  }

  getAccess() {
    return Cookies.get('access');
  }

  getUser() {
    return Cookies.get('user');
  }

  setCookies(token: string, user: string, expires: Object): void {
    Cookies.set('access', token, expires);
    Cookies.set('user', user, expires);
  }

  deleteCookies(): void {
    Cookies.remove('access');
    Cookies.remove('user');
  }
}

export default new SessionCookie();
