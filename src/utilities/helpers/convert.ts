export const convert = (name: string, key: string) => {
  const state: any = {
    roles: [
      { key: 'ADMIN', name: 'Адміністратор' },
      { key: 'USER', name: 'Користувач' },
    ],
    statuses: [
      { key: 'ACTIVE', name: 'Онлайн' },
      { key: 'DEACTIVATED', name: 'Деактивовано' },
      { key: 'INVITATION_SENT', name: 'Надіслано запрошення' },
      { key: 'PASSWORD_RESTORE', name: 'Відновлення запрошення' },
    ],
  };

  return state[key].find((i: any) => i.key === name).name;
};
