const getHighlight = (findText: string, name: string) => name.slice(findText.length);

export default getHighlight