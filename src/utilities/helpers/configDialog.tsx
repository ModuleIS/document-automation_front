import React from 'react';
import { CloseOutlined } from '@ant-design/icons';

export const config = {
  icon: null,
  closeIcon: <CloseOutlined />,
  closable: true,
  maskClosable: true,
  width: 520,
};
