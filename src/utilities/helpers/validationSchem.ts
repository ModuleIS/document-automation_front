import * as Yup from 'yup';
import 'yup-phone';

export const signInSchema = Yup.object().shape({
  firstname: Yup.string().required("обов'язково до заповнення"),
  patronymic: Yup.string().required("обов'язково до заповнення"),
  secondname: Yup.string().required("обов'язково до заповнення"),
  password: Yup.string().required("обов'язково до заповнення"),
});

export const recoveryPasswordSchema = Yup.object().shape({
  email: Yup.string()
    .email('перевірте правильність написання email')
    .required("обов'язково до заповнення"),
});

export const createUserSchema = Yup.object().shape({
  firstname: Yup.string().required("обов'язково до заповнення"),
  patronymic: Yup.string().required("обов'язково до заповнення"),
  secondname: Yup.string().required("обов'язково до заповнення"),
  position: Yup.string().required('виберіть посаду'),
  role: Yup.string().required('виберіть роль'),
  phone: Yup.string()
    .required("обов'язково до заповнення")
    .phone('UA', true, 'Номер телефону недійсний'),
  email: Yup.string()
    .email('перевірте правильність написання email')
    .required("обов'язково до заповнення"),
  password: Yup.string()
    .min(8, 'все ж таки, не менше 8 символів')
    .required("обов'язково до заповнення"),
});

export const createAccessGroup = Yup.object().shape({
  name: Yup.string().required("обов'язково до заповнення"),
  descriptions: Yup.string(),
  members: Yup.string().required("обов'язково до заповнення"),
  position: Yup.string().required('виберіть посаду'),
  documents: Yup.string().required('виберіть документ '),
  folders: Yup.string().required('виберіть теку '),
  access: Yup.string().required('виберіть доступ '),
});

// .min(5, 'все ж таки, не менше 8 символів')
// .matches(/^(?=.*[A-z])(?=.*[0-9]).{8,}$/, 'пароль повинен містити літери та цифри')
// .matches(/^(?=.*[A-z])(?=.*[0-9]).{8,}$/, 'пароль повинен містити літери та цифри')
