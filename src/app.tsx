import React from 'react';
import { Switch } from 'react-router-dom';

import Dashboard from 'views/pages/Dashboard';
import SignIn from 'views/components/Auth/SignIn';
import ResetPassword from 'views/components/Auth/ResetPassword';
import { PublicRoute, PrivateRoute } from 'views/routes';

const App = () => (
  <Switch>
    <PublicRoute exact path="/auth/sign_in" component={SignIn} />
    <PublicRoute exact path="/auth/reset_password" component={ResetPassword} />
    <PrivateRoute path="/" component={Dashboard} />
  </Switch>
);

export default App;
